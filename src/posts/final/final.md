---
visibility: true
weekId: 18
title: "Final Project - Inner Voice"
publishDate: "2020-05-27"
weekImg: "18-final_Inner_Voice.jpg"
tags: "#Wireless Interactive Wearable Technology"
assignment: "Complete your final project tracking your progress."
download: "https://github.com/cv47522/Inner-Voice_ESP32_WS2812B_Interactive_Wearable_Technology"
---
```toc
# This code block gets replaced with the Table of Content
```

---

## Final Idea: Inner Voice - Wireless Interactive Wearable Technology

### [Link to Final Project Build Page](https://fabacademy.wantinghsieh.com/assignment/16-wildcard-week/)

---

## Initial Idea: Breath of Flora - Interactive Sound and Light Mechanical Flower

### [Link to Final Project Proposal Page](https://fabacademy.wantinghsieh.com/assignment/10-applications-and-Implications)

_“Breath of Flora” is an interactive sound and light mechanical flower. Infrared sensors and sound detection sensors are used in the artwork as the main technique to interact with people. The work adopts “Listening” as the concept and is going to be displayed in the oldest and longest functioning mental hospital in Finland —— Lapinlahti Hospital around April 2020._

_Wandering in the Historical Garden at Lapinlahti Mental Hospital in Finland. In the dim quiet passage full of kinetic flowers, these blossoms act as if they were transformed from consciousness of the past sufferers living with perception, leading audiences to listen and feel the patients' apprehension and belongingness deep inside their hearts._

### Concept

#### Background

Since Lapinlahti mental hospital was rather advanced for its time, surrounded by the sea and large gardens to calm and cure patients. From the beginning, gardening was seen as a key part of therapy and nature as a soothing place to relieve stress. Today Lapinlahti hospital is a communal living room open for everyone. It functions as an oasis of culture, art and events and as a haven of wellbeing for the mind and body.

![hospital-1.jpg](./img/hospital-1.jpg#width)

![hospital-2.jpg](./img/hospital-2.jpg#width)

#### Sketch & Movement Reference

![flower-sketch.jpg](./img/flower-sketch.jpg)
[[gifSize]]
| ![flower-movement.gif](./img/flower-movement.gif)

![hospital-1.jpg](./img/hospital-1.jpg)

![hospital-1.jpg](./img/hospital-1.jpg)

#### Features

- **Art + Technology**
  By combining art with technology, it actively educates audiences to put themselves in patients’ shoes and introspect whether if they have the right attitude to hearken to sick people, which enhances the connection between new media art and psychiatric hospitals.

- **Dynamic v.s. Static**
  Compared to still sculptures decorated in the majority of public space, the kinetic installation is made from electronic components and programmed with codes for the sake of returning sound and light feedback depending on the length and noise level during the interaction.

- **Participatory Art**
  Since the way of how every passerby interacts with the artwork is unique, the installation has also become a kind of participatory art which engages public participation in the innovative process, letting them become co-authors, editors, and observers of the work and is deficient without audiences' physical interaction, making it into an exclusive artwork ultimately.

#### Inspiration

`youtube: mwue6cvG6aY`

`youtube: _hzQIuPzaBM`
