---
visibility: true
weekId: 9
title: "Input Devices"
publishDate: "2020-04-24"
weekImg: "09-input-devices.png"
tags: "#ATtiny1614 #Phototransistor #Infrared(IR) Emitter"
assignment: "(Group) 1. Probe an input device(s)'s analog and digital signals. (Individual) 2. Document your work. 3. Measure something: add a sensor to a microcontroller board that you have designed and read it."
download: "https://gitlab.com/cv47522/kicad-attiny1614_a4953_stepper_audio_phototransistor"
---
```toc
# This code block gets replaced with the Table of Content
```

---

## Group Assignment: Probe an input device(s)'s analog and digital signals

### [Link to Documentation Page](https://aaltofablab.gitlab.io/fab-academy-2020/input-devices/)

I used the built-in serial monitor of Arduino IDE to observe my IR receiving sensor made with an NPN-phototransistor. From the code of the [Sensor Reading](#sensor-reading) section, the state value of the sensor is digital, which means that it will only print either **1(HIGH)** or **0(LOW)** in the serial monitor.

![IR_emitter_receiver.png](./img/IR_emitter_receiver.png)

---

## Individual Assignment: Add a sensor to a microcontroller

### Materials

<div style='overflow-x:auto;'>
    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Source</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>ATtiny1614 * 1</td>
                <td>
                    <a href="https://www.digikey.fi/product-detail/en/microchip-technology/ATTINY1614-SSFR/ATTINY1614-SSFRCT-ND/7354422" target='_blank'>DigiKey</a>
                </td>
                <td>The main microcontroller chip.</td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>Phototransistor</td>
                <td>
                    <a href="https://www.digikey.fi/product-detail/en/everlight-electronics-co-ltd/PT15-21C-TR8/1080-1380-1-ND/2676114" target='_blank'>DigiKey</a>
                </td>
                <td>
                    Used as an infrared(IR) receiver.
                </td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>LED * 4</td>
                <td>-</td>
                <td>
                    Used for monitoring power/RXTX and programming.
                </td>
            </tr>
            <tr>
                <th scope="row">4</th>
                <td>Headers</td>
                <td>-</a>
                </td>
                <td>
                    2.54mm male/female headers & a barrel jack power header, .
                </td>
            </tr>
            <tr>
                <th scope="row">5</th>
                <td>Infrared(IR) Lamp/Emitter</td>
                <td>-</td>
                <td>
                    Used for triggering the phototransistor to send signal to the host board.
                </td>
            </tr>
            <tr>
                <th scope="row">6</th>
                <td>Capacitors</td>
                <td>-</td>
                <td>-</td>
            </tr>
            <tr>
                <th scope="row">7</th>
                <td>Resistors</td>
                <td>-</td>
                <td>-</td>
            </tr>
            <tr>
                <th scope="row">8</th>
                <td>Jumper Wires</td>
                <td>-</td>
                <td>-</td>
            </tr>
        </tbody>
    </table>
</div>

### Build

1. PCB Design: **Learn more from [Week 6: Electronics Design](http://fabacademy.wantinghsieh.com/assignment/06-electronics-design/)**

2. PCB Milling: **Learn more from [Week 4: Electronics Production](http://fabacademy.wantinghsieh.com/assignment/04-electronics-production/)**

### Programming ([Code](https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny1614_phototransistor))

Programming Environment Setup: **Learn more from [Week 8: Embedded Programming](http://fabacademy.wantinghsieh.com/assignment/08-embedded-programming/)**

#### Phototransistor Usage

The following video I watched provides some calculation about how to design an infrared receiving sensor with an NPN-phototransistor or a photodiode plus an NPN-transistor:

`youtube: zx4OUXk0LSQ`

![photodiode_phototransistor.png](./img/photodiode_phototransistor.png)

Although the circuit looks simple, the value of the resistor used here affects the sensitivity of the sensor. I followed the tutorial to design my circuit in KiCad.

![phototransistor_circuit.png](./img/phototransistor_circuit.png)

![sch_phototransistor.png](./img/sch_phototransistor.png)

In the circuit, we care most about the digital output value on **PHOTO** pin (**V<sub>PHOTO</sub>**) which will output either **3.3V(HIGH)** or **0V(LOW)** to the serial monitor of the Arduino IDE. I summarized which value will be outputted under different scenarios.

[[eqBlock]]
| Digital Output on V<sub>PHOTO</sub> = 0V(LOW) or 3.3V(HIGH)<br />
| <br />
| In Bright Space: V<sub>PHOTO</sub> = 0V(LOW), Why: R<sub>1</sub> consumes most of 3.3V<br />
| → Ohms Law: V = I * R <br />
| → 3.3V = 10k Ω * I <br />
| → I = 0.33mA <br />
| <br />
| In Dark Space: V<sub>PHOTO</sub> = 3.3V(HIGH), Why: No current goes through R<sub>1</sub> <br />
| → No voltage drop on R<sub>1</sub>

#### Pin Definition

Here I defined 4 pins for programming: two of them (**RX**, **TX**) are for communicating with the serial monitor, another one (**PHOTO_AVR**) is the IR sensor input pin and the other one (**LED_AVR**) is the LED output for monitoring the sensor state.

```arduino
#include <avr/io.h>
#include <util/delay.h>
#include <SoftwareSerial.h>
//Check out pins from ~/Library/Arduino15/packages/megaTinyCore/hardware/megaavr/2.0.1/variants/txy4/pins_arduino.h
#define RX    9   // *** PA2
#define TX    8   // *** PA1

SoftwareSerial mySerial(RX, TX);

#define LED_AVR PIN4_bm // PA4:LED pin
#define PHOTO_AVR PIN7_bm // Phototransistor sensor

void setup() {
  mySerial.begin(115200);
  PORTA.DIRSET = LED_AVR; //Output
  PORTA.DIRCLR = PHOTO_AVR; // Input
}

```

#### Sensor Reading

Inside `void loop()`, I used `PORTA.IN & PHOTO_AVR` to read the digital value (**1 = HIGH** or **0 = LOW**) of the IR sensor and stored it to the `state` variable. Within `switch/case`, I defined when to blink a LED denpending on the value of the sensor state.

```arduino
void loop() {
  bool state = PORTA.IN & PHOTO_AVR; // read the state of the phototransistor
  
  switch(state) {
    case 1:
    PORTA.OUT &= ~LED_AVR; // turn the LED OFF
    break;
    case 0:
    PORTA.OUT |= LED_AVR; // turn the LED ON
    break;
    default:
    PORTA.OUT &= ~LED_AVR; // turn the LED OFF
    break;
  }

  mySerial.println(state); // print state value in the serial monitor: 1(HIGH) or 0(LOW)
  delay(200);
 }
```

The IR emmiter can be an infrared lamp or LED. In the video, I adpoted an IR module and connected its power pins to an Arduino board without programming and then used it to test if my custom IR receiving sensor works. We can see that the LED is blinked once the phototransistor detects nearby infrared light.

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/Phototransistor_IR_Emitter_small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

### Problem & Solution

Since the package of a SMT phototransistor is similar to a SMT LED, it is easier to place it in the wrong direction.

#### Before Correction

From the diagrams below, the **cathode** pin (usually connected to **GND**) of a SMT LED is **marked in green** whereas the **collector** pin (usually connected to a **resistor** or **V<sub>cc</sub>**) of a SMT NPN-phototransistor is also **marked in green**, which makes it confusing during soldering. The IR receiving circuit didn't work with the phototransistor placed in the wrong direction.

![LED_mark.jpg](./img/LED_mark.jpg)

![collector_mark.png](./img/collector_mark.png)

![incorrect_IR_receiver.jpg](./img/incorrect_IR_receiver.jpg)

![incorrect_IR_receiver_part.jpg](./img/incorrect_IR_receiver_part.jpg)

![PCB_IR_receiver.png](./img/PCB_IR_receiver.png)

#### After Correction

After reversing its pin drection with two jumper wires, the IR receiving sensor starts to act normally.

![correct_IR_receiver.jpg](./img/correct_IR_receiver.jpg)