---
visibility: true
weekId: 12
title: "Invention, Intellectual Property and Incomek"
publishDate: "2020-10-22"
weekImg: "12-invention.png"
tags: "#Project Video #Project Slides"
assignment: "(Individual) 1. Develop a plan for dissemination of your final project. 2. Prepare drafts of your summary slide and video clip."
download: "https://gitlab.com/cv47522/fab-academy/-/tree/master/src/posts/12-invention/source_files"
---
```toc
# This code block gets replaced with the Table of Content
```

---

## Individual Assignment: Wireless Interactive Wearable Technology

### Project Video

`youtube: FY3dla8zZSA`

### Description

This project is called "Inner Voice" which is an e-textile project consisting of a glove connected to a bracelet as a signal sender, 8 pieces of light modules in tetrahedron shapes installed on a dress and a belt as a signal receiver. With "Inner Voice", we can express self-conscious emotions in a more polite way without ruining others’ moods whether in a party or a public space simply by using different kinds of hand gestures to light up various patterns remotely. For this project, I replaced all the jumper wires and breadboards with conductive fabrics, yarns and self-made PCB shields to make everything in order.

### Concept

Sometimes there is a situation that it is embarrassing or difficult to talk about personal feelings through plain words during a conversation.
Is there any way to inform others about self discomfort without offending them?
According to this, I came up with an idea that is it possible to express ourselves through light controlled by our gestures?

![Inner_Voice_concept_1_web.png](./img/Inner_Voice_concept_1_web.png)

For example, by defining certain hand gestures and matching them to specific light patterns, people can understand our current thinking, stop continuing bothering us and leave us alone for getting better.

![Inner_Voice_design_1_web.png](./img/Inner_Voice_design_1_web.png)

### Slides

#### [Link to Final Project Slides](https://gitlab.com/cv47522/fab-academy/-/tree/master/src/posts/12-invention/source_files)

### Build

#### [Link to Final Project Build Page](https://fabacademy.wantinghsieh.com/assignment/16-wildcard-week)
