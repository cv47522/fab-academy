---
visibility: true
weekId: 3
title: "Computer-Controlled Cutting"
publishDate: "2020-02-12"
weekImg: "03-vinylcutter-heat-9.jpg"
tags: "#OpenSCAD #Illustrator #Laser Cutting #Vinyl Cutting"
assignment: "(Group) 1. Characterise your laser cutter focus, power, speed, rate, kerf, and joint clearance. (Individual) 2. Design, laser cut, and document a parametric press-fit construction kit, which can be assembled in multiple ways. Account for the laser cutter kerf. 3. For extra credit include elements that aren't flat."
download: "https://gitlab.com/cv47522/fab-academy/-/tree/master/src/posts/03-computer-controlled-cutting/source_files"
---
```toc
# This code block gets replaced with the Table of Content
```

---

## Individual Assignment: Vinyl Cutting Project

![vinylcutter-heat-7.jpg](./img/vinylcutter-heat-7.jpg)

### Materials

<div style="overflow-x:auto;">
    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">1</th>
                <td>Vinyl Cutter: Roland GX-24 (<a href='https://www.rolanddga.com/support/products/cutting/camm-1-gx-24-24-vinyl-cutter'>Manual</a>)</td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>Heat Press Machine</td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>T-Shirt Heat Transfer Stickers</td>
            </tr>
        </tbody>
    </table>
</div>

### Setup: Vector Image Adjustment in Illustrator

I used my vector painting image as the material for vinyl cutting.

![vinylcutter-setup-1.png](./img/vinylcutter-setup-1.png)

![vinylcutter-setup-2.png](./img/vinylcutter-setup-2.png)

Before cutting, I did some adjustment so that the path of the blade could work as my expectation. In order to simplify the vectors of my image, I chose which layer is going to be trimmed with another one. I did this by clicking the **Trim** option of the **Pathfinders** dialogue box in Illustrator.

![vinylcutter-setup-3.png](./img/vinylcutter-setup-3.png)

![vinylcutter-setup-5.png](./img/vinylcutter-setup-5.png)

After this, I refilled the color of different parts and grouped parts with the same color.

![vinylcutter-setup-4.png](./img/vinylcutter-setup-4.png)

![vinylcutter-setup-6.png](./img/vinylcutter-setup-6.png)

### Fabrication: Roland GX-24 Vinyl Cutter

The finished file was then sent to the machine wainting for cutting.
First, I measured the width of the inserted material sheet by selecting the **EDGE** mode.

![vinylcutter-cut-0.jpg](./img/vinylcutter-cut-0.jpg)

![vinylcutter-cut-1.jpg](./img/vinylcutter-cut-1.jpg)

![vinylcutter-cut-2.jpg](./img/vinylcutter-cut-2.jpg)

Then, since I was going to cut the same image 6 times with 6 colors, I chose **Output Selected Lines** option, selected the first mirrored layer and clicked **Output the Paths**.

![vinylcutter-cut-3.jpg](./img/vinylcutter-cut-3.jpg)

![vinylcutter-cut-4.jpg](./img/vinylcutter-cut-4.jpg)

![vinylcutter-cut-5.jpg](./img/vinylcutter-cut-5.jpg)

After the cutting was done, I pressed **MENU** then **ENTER** button to exit the cutting mode.

![vinylcutter-cut-6.jpg](./img/vinylcutter-cut-6.jpg)

![vinylcutter-cut-7.jpg](./img/vinylcutter-cut-7.jpg)

![vinylcutter-cut-8.jpg](./img/vinylcutter-cut-8.jpg)

I kept the parts I would like to transfer to a T-shirt.

![vinylcutter-layer-0.jpg](./img/vinylcutter-layer-0.jpg)

![vinylcutter-layer-1.jpg](./img/vinylcutter-layer-1.jpg)

![vinylcutter-layer-2.jpg](./img/vinylcutter-layer-2.jpg)

![vinylcutter-layer-3.jpg](./img/vinylcutter-layer-3.jpg)

### T-Shirt Heat Transfer

Finally, we used a heat press machine for transfering the cutting image to a T-shirt. The machine should be heated up to 165℃ to make sure it can transfer the pattern successfully. Since there are total 6 layers I would like to transfer to the T-shirt, I pressed each layer (except the last yellow layer) and waited for 3 seconds then changed to the next one (normally 12 seconds for total 1 layer).

![vinylcutter-heat-1.jpg](./img/vinylcutter-heat-1.jpg)

![vinylcutter-heat-2.jpg](./img/vinylcutter-heat-2.jpg)

![vinylcutter-heat-3.jpg](./img/vinylcutter-heat-3.jpg)

![vinylcutter-heat-4.jpg](./img/vinylcutter-heat-4.jpg)

![vinylcutter-heat-5.jpg](./img/vinylcutter-heat-5.jpg)

![vinylcutter-heat-6.jpg](./img/vinylcutter-heat-6.jpg)

![vinylcutter-heat-7.jpg](./img/vinylcutter-heat-7.jpg)

![vinylcutter-heat-8.jpg](./img/vinylcutter-heat-8.jpg)

I was lucky to find a reflecting sheet in a trash box, which results in a cool T-shirt with a fancy cat.

![vinylcutter-heat-10.jpg](./img/vinylcutter-heat-10.jpg)

---

## Group Assignment: Characterize Our Laser Cutter

### [Link to Documentation Page](https://aaltofablab.gitlab.io/fab-academy-2020/computer-controlled-cutting/)

### Materials

<div style="overflow-x:auto;">
    <table>
        <thead>
            <tr>
            <th>#</th>
            <th>Name</th>
            <th>Type</th>
            <th>(Cutting) Area</th>
            <th>Height</th>
            <th>Other</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row">1</th>
            <td>Laser Cutter: EPILOG Legend 36EXT (<a href='https://www.epiloglaser.com/downloads/pdf/ext_4.22.10.pdf'>Manual</a>)</td>
            <td>60W CO<sub>2</sub></td>
            <td>914 x 610mm</td>
            <td>305mm</td>
            <td><ul>
                <li>kerf: 0.125mm</li>
                <li>supporting file formats: .ai, .cdr, .pdf, and .svg</li>
                <li><a href='https://wiki.aalto.fi/display/AF/Laser+Cutter+Epilog+Legend+36EXT'>Fablab Step-by-Step Guideline</a></li>
            </ul></td>
            </tr>
            <tr>
            <th scope="row">2</th>
            <td>Laser Cutting Sheets</td>
            <td>MDF</td>
            <td>60 x 60mm</td>
            <td>4mm</td>
            <td></td>
            </tr>
        </tbody>
    </table>
</div>

---

## Individual Assignment: Laser Cutting Project 1 - Parametric Press-Fit Kit

![lasercut-final-2.jpg](./img/lasercut-final-2.jpg)

### Material Sheets + Cutting Parameters

<div style="overflow-x:auto;">
    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Area</th>
                <th>Thickness</th>
                <th>Speed</th>
                <th>Power</th>
                <th>Frequency</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">1</th>
                <td>MDF</td>
                <td>300 x 200mm</td>
                <td>6mm</td>
                <td>8%</td>
                <td>75%</td>
                <td>500Hz</td>
            </tr>
            </tbody>
    </table>
</div>

### Design Tool: OpenSCAD

I defined 3 modules (**base_shape()**, **sheet_kerf()** and **chamfer_kerf()**) and differenced them to form the final combination. I can transform some features by simply changing the values of **laser\_kerf\_width**, **sheet_thickness**, **sheet\_kerf\_length**, etc.

![lasercut-openscad-1.png](./img/lasercut-openscad-1.png)

It is convenient to use a **for** loop function in OpenSCAD to generate multiple units at once.

![lasercut-openscad-2.png](./img/lasercut-openscad-2.png)

### Cutting Tool: Illustrator

It is important to make sure that the **stroke width** of all the cutting vectors is set to **0.001 with #000000 black color**.

![lasercut-setup-1.png](./img/lasercut-setup-1.png)

Then I opened the **Print** dialogue box and did the following settings step by step.

![lasercut-setup-2.png](./img/lasercut-setup-2.png)

![lasercut-setup-3.png](./img/lasercut-setup-3.png)

![lasercut-setup-5.png](./img/lasercut-setup-5.png)

Here is [a table for the setting of Fablab's laser](https://wiki.aalto.fi/display/AF/Laser+Cutter+Settings) cutter according to the thickness and the material of a cutting sheet.

![lasercut-setup-4.png](./img/lasercut-setup-4.png)

Below is the documentation of the laser cutting process:

![lasercut-cutting-1.jpg](./img/lasercut-cutting-1.jpg)

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/lasercut-cutting-0-small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

### End Result

![lasercut-final-1.jpg](./img/lasercut-final-1.jpg)

![lasercut-final-2.jpg](./img/lasercut-final-2.jpg)

![lasercut-final-3.jpg](./img/lasercut-final-3.jpg)

---
## Individual Assignment: Laser Cutting Project 2 - Living Hindge

![lasercut-living-hinge-1.jpg](./img/lasercut-living-hinge-1.jpg)

### Material Sheets + Cutting Parameters

<div style="overflow-x:auto;">
    <table>
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Area</th>
            <th>Thickness</th>
            <th>Speed</th>
            <th>Power</th>
            <th>Frequency</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>1</td>
            <td>Paper</td>
            <td>150 x 150mm</td>
            <td>0.2mm</td>
            <td>90%</td>
            <td>55%</td>
            <td>500Hz</td>
        </tr>
        </tbody>
    </table>
</div>

### Design Tool: OpenSCAD

Since it is normal to use linear pattern to build a living hinge, I was intereseted in exploring more possibilities to construct other kinds of living hinges. I found some discussion about how to build a living hinge from the [OpenSCAD forum](http://forum.openscad.org/Living-Hinges-td18217.html). I then used 4 modules as a reference and experimented them by transforming them and changing their parameters.

![lasercut-openscad-3.png](./img/lasercut-openscad-3.png)

### End Result

The sheet with different patterns has distinct flexibility.

[[gifSize]]
| ![lasercut-living-hinge-test-1.gif](./img/lasercut-living-hinge-test-1.gif)
| ![lasercut-living-hinge-test-2.gif](./img/lasercut-living-hinge-test-2.gif)
| ![lasercut-living-hinge-test-3.gif](./img/lasercut-living-hinge-test-3.gif)
| ![lasercut-living-hinge-test-4.gif](./img/lasercut-living-hinge-test-4.gif)

Below is the documentation of the laser cutting process:

![lasercut-living-hinge-1.jpg](./img/lasercut-living-hinge-1.jpg)

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/lasercut-living-hinge-snail-small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
