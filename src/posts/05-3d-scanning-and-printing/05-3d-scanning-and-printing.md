---
visibility: true
weekId: 5
title: "3D Scanning and Printing"
publishDate: "2020-02-26"
weekImg: "05-3DP_hero-shot.jpg"
tags: "#Fusion360 #Cura #Ultimaker 2+ Extended"
assignment: "(Group) 1. Test the design rules for your 3D printer(s). 2. Document your work and explain what are the limits of your printer(s). (Individual) 3. Design and 3D print an object (small, few cm3, limited by printer time) that could not be made subtractively. 4. 3D scan an object (and optionally print it)."
download: "https://gitlab.com/cv47522/fab-academy/-/tree/master/src/posts/05-3d-scanning-and-printing/source_files"
---
```toc
# This code block gets replaced with the Table of Content
```

---

## Group Assignment: Test the design rules for your 3D printer(s)

### [Link to Documentation Page](https://aaltofablab.gitlab.io/fab-academy-2020/3d-scanning-and-printing/)

---

## Individual Assignment: 3D Printing

### Materials

<div style='overflow-x: auto;'>
    <table>
        <thead>
            <tr>
            <th>#</th>
            <th>Name</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row">1</th>
            <td>3D Printer: Ultimaker 2+ Extended (<a href='https://support.ultimaker.com/hc/en-us/articles/360011811480-The-Ultimaker-2-user-manuals' target='_blank'>Manual</a>)</td>
            </tr>
            <tr>
            <th scope="row">2</th>
            <td>1.75mm PLA Filament</td>
            </tr>
        </tbody>
    </table>
</div>

### [CURA](https://ultimaker.com/software/ultimaker-cura) Software Setup

I downloaded an open-source **STL** model from [Thingiverse](https://www.thingiverse.com/) for testing the limitation of our printer.

![3D_printer_test_model.png](./img/3D_printer_test_model.png)

 After importing the model to CURA, I did some correspondent settings within the software before converting the **STL** file to **gcode** file for printing:

1. Set the **Layer Height** to **0.2mm**. (ps. The printing time is longer with smaller layer height/resolution.)
![u2+_step-1_layer_height.png](./img/u2+_step-1_layer_height.png)

2. Set the **Wall Thickness** to **1mm**. (ps. The printing time is longer with thicker walls.)
![u2+_step-2_shell_1mm.png](./img/u2+_step-2_shell_1mm.png)

3. Set the **Infill Density** to **15%**. (ps. The printing time is longer with more infill desity.)
![u2+_step-3_infill.png](./img/u2+_step-3_infill.png)

4. Set the **Build Plate Adhesion Type** to **Skirt** or **Brim**. (ps. The printing time is longer with **Raft**.)
![u2+_step-4_skirt.png](./img/u2+_step-4_skirt.png)

5. Set the **Print Speed** to **150mm/s** which is within the range indicated in the datasheet. After every setting is done, click **Preview** to double check to nozzle movement with simulation.
![u2+_step-5_speed_150.png](./img/u2+_step-5_speed_150.png)

![u2+_extended_max_print_speed.png](./img/u2+_extended_max_print_speed.png)

6. Click **Save to Removable Device** to export the **gcode** file to a USB flash drive for standalone printing.
![u2+_step-6_preview_mode.png](./img/u2+_step-6_preview_mode.png)

### Printing Steps

![Ultimaker_2+_Extended.jpg](./img/Ultimaker_2+_Extended.jpg)

Here are the steps about how to use **Ultimaker 2+ Extended** to print models with single color after exporting the **gcode** file:

1. Power on the printer.
![step-1-power_on.jpg](./img/step-1-power_on.jpg)

2. Select **Material** in the menu to change the filament.
![step-2-menu_material.jpg](./img/step-2-menu_material.jpg)

3. Click **Change**.
![step-3-menu_change.jpg](./img/step-3-menu_change.jpg)

4. Wait for the printer to heat up its nozzle.
![step-4-menu_heat_up.jpg](./img/step-4-menu_heat_up.jpg)

5. Choose which filament to use for printing.
![step-5-filament.jpg](./img/step-5-filament.jpg)

6. Click **Ready** to let the printer extract the original filament.
![step-6-menu_remove_material.jpg](./img/step-6-menu_remove_material.jpg)

7. Pull out the filament.
![step-7-remove_pla.jpg](./img/step-7-remove_pla.jpg)

8. Use the tool to cut the head of the filament to flat.
![step-8-cut_pla.jpg](./img/step-8-cut_pla.jpg)
![step-8-flat_pla.jpg](./img/step-8-flat_pla.jpg)

9. Insert the filament in to the hole manually.
![step-9-insert_pla.jpg](./img/step-9-insert_pla.jpg)

10. Select the type of the filament (Here: **PLA**).
![step-10-menu_pla.jpg](./img/step-10-menu_pla.jpg)

11. Click **Ready** to let the printer suck the filament.
![step-11-menu_ready.jpg](./img/step-11-menu_ready.jpg)

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/suck_pla_small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

12. Click **Ready** after the printer extrudes the filament from its nozzle.
![step-12-menu_wait.jpg](./img/step-12-menu_wait.jpg)

13. Back to the home menu and click **Print**.
![step-13-menu_print.jpg](./img/step-13-menu_print.jpg)

14. Select a **gcode** file for printing.
![step-14-menu_select_file.jpg](./img/step-14-menu_select_file.jpg)

15. Wait for the printer to heat up.
![step-15-menu_heat_up.jpg](./img/step-15-menu_heat_up.jpg)

16. The printer is printing the first layer. The filament I inserted is white whereas the leftover of the previous filament is blue.

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/print_white_model_small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

17. Remove the model with a blunt knife.
![step-17-finish_printing.jpg](./img/step-17-finish_printing.jpg)
![step-17-remove_model.jpg](./img/step-17-remove_model.jpg)

### Single-Color Model (Ultimaker 2+ Extended)

![model_white_side_3.jpg](./img/model_white_side_3.jpg)

![model_white_top.jpg](./img/model_white_top.jpg)

![model_white_part_1.jpg](./img/model_white_part_1.jpg)

Since the white model was too bright to see the printing details, I re-printed the same model with orange filament.
<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/print_orange_model_small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

![model_orange_white_side_1.jpg](./img/model_orange_white_side_1.jpg)

We can see from the diagrams that the filament starts to fall when the printing angle is greater than **70 degrees**.
![model_orange_side_1.jpg](./img/model_orange_side_1.jpg)

![model_orange_side_2.jpg](./img/model_orange_side_2.jpg)

![model_orange_top_2.jpg](./img/model_orange_top_2.jpg)

### Dual-Color Model (Ultimaker 3 Extended)

I also tried to print a set of models in dual colors at once with [Ultimaker 3 Extended](https://ultimaker.com/3d-printers/ultimaker-3).

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/print_pink_blue_model_small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

![model_pink_blue_side_1.jpg](./img/model_pink_blue_side_1.jpg)

---

## Individual Assignment: 3D Scanning

### Materials

<div style='overflow-x: auto;'>
    <table>
        <thead>
            <tr>
            <th>#</th>
            <th>Name</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row">1</th>
            <td><a href='https://www.artec3d.com/portable-3d-scanners/artec-eva-v2' target='_blank'>Artect 3D Scanner</a></td>
            </tr>
            <tr>
            <th scope="row">2</th>
            <td>A 3D object for scanning</td>
            </tr>
        </tbody>
    </table>
</div>

### Artect Studio Software Setup ([Manual](https://gitlab.com/cv47522/fab-academy/-/tree/master/src/posts/05-3d-scanning-and-printing/source_files/Artect3DScanner-Manual-11.1.0-EN.pdf)) & Scanning Steps

1. Open the application.
![3dScan_step1-open_3D_Scan_IDE.png](./img/3dScan_step1-open_3D_Scan_IDE.png)

2. Click the **Scan** button on the sidebar.
![3dScan_step2-click_scan_button.png](./img/3dScan_step2-click_scan_button.png)

3. Connect the 3D scanner to a computer.
![3dScan_step3-connect_scanner.jpg](./img/3dScan_step3-connect_scanner.jpg)

4. After detecting the 3D scanner, check the **Real-time fusion** and click **Preview**.
![3dScan_step4-connect_USB.png](./img/3dScan_step4-connect_USB.png)

5. Hold the scanner and palce the model on a spinning table.
![3dScan_step5-hold_scanner.jpg](./img/3dScan_step5-hold_scanner.jpg)

6. Click **Record** to start scanning.
![3dScan_step6-preview.png](./img/3dScan_step6-preview.png)

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/3D_scan_small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

7. Click **Stop** to end scanning and start to visualize the model in the application.
![3dScan_step7_record.png](./img/3dScan_step7_record.png)

![3dScan_step7_stop.png](./img/3dScan_step7_stop.png)

8. The model with multiple layers.
![3dScan_step8-multi_layers.png](./img/3dScan_step8-multi_layers.png)

9. Only activate the **Fusion** layer.
![3dScan_step9-active_only_fusion_layer.png](./img/3dScan_step9-active_only_fusion_layer.png)

![3dScan_step9-front_view.png](./img/3dScan_step9-front_view.png)

10. Remove unwanted area.  
  a. Method 1: Click **Editor** on the sidebar to erase nosies manually.
  ![3dScan_step10-erase_rectagle_selection.png](./img/3dScan_step10-erase_rectagle_selection.png)

  b. Method 2: Click **Small-object filter** within the **Tools** sub-menu to automatically remove nosies.
  ![3dScan_step10-smallObjFilter.png](./img/3dScan_step10-smallObjFilter.png)

11. Click **Hole filling** to complete the model.
![3dScan_step11-holeFilling.png](./img/3dScan_step11-holeFilling.png)

12. Export the scanning model as a **STL** file for 3D printing.
![3dScan_step12-exportMeshesSTL.png](./img/3dScan_step12-exportMeshesSTL.png)

13. Check the **STL** file in a 3D viewer.
![3dScan_step13-viewExportFile.png](./img/3dScan_step13-viewExportFile.png)