---
visibility: true
weekId: 17
title: "Mechanical and Machine Design"
publishDate: "2020-09-28"
weekImg: "17-machine_1_web.jpg"
tags: "#Node.js #Johnny-five #Stepper Motor #Arduino #Machine Building"
assignment: "(Group) 1. Actuate and automate your machine. 2. Document the group project. (Individual) 3. Document your individual contribution."
download: "https://gitlab.com/cv47522/nodejs-j5-arduino-stepper"
---
```toc
# This code block gets replaced with the Table of Content
```

---

## Group Assignment: Document the group machine project

### [Link to Documentation Page](https://aaltofablab.gitlab.io/fab-academy-2020/machine-building/)

---

## Individual Contribution: Control Software/Web Interface and Serial Bridge

`youtube: y3VsqAG2W88`

### Materials

<div style='overflow-x:auto;'>
    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td><a href='https://gitlab.com/cv47522/nodejs-j5-arduino-stepper' target='_blank'>Application Repository</a></td>
                <td> Used as a web interface for controlling the machine through the serial port.</td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>Stepper Driver (<a href='https://www.digikey.fi/product-detail/en/trinamic-motion-control-gmbh/TMC2208-SILENTSTEPSTICK/1460-1201-ND/6873626' target='_blank'>TMC2208 SilentStepStick</a> or <a href='https://www.pololu.com/product/1182' target='_blank'>Pololu A4988</a>) * 1
                </td>
                <td>
                    Used for easily controling a stepper motor.
                </td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>Arduino UNO * 1</td>
                <td>The main controller for the machine.</td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>Stepper Driver (<a href='https://www.digikey.fi/product-detail/en/trinamic-motion-control-gmbh/TMC2208-SILENTSTEPSTICK/1460-1201-ND/6873626' target='_blank'>TMC2208 SilentStepStick</a> or <a href='https://www.pololu.com/product/1182' target='_blank'>Pololu A4988</a>) * 1
                </td>
                <td>
                    Used for controlling one stepper motor.
                </td>
            </tr>
            <tr>
                <th scope="row">4</th>
                <td>Stepper Motor (Bipolar, or 4-wire) * 1</td>
                <td>
                    Used for rotating the circular plate.
                </td>
            </tr>
            <tr>
                <th scope="row">5</th>
                <td>8-35V Power Adapter/Supply</td>
                <td>Used for providing external power source with larger current to the stepper driver.</td>
            </tr>
            <tr>
                <th scope="row">6</th>
                <td><a href="https://www.sparkfun.com/products/119" target='_blank'>5.5mm Barrel Power Jack</a>
                </td>
                <td>Used for connecting to the power adapter.</td>
            </tr>
            <tr>
                <th scope="row">7</th>
                <td>Optional: <a href="https://github.com/jw4rd/stepper" target='_blank'>DIY Stepper Shield</a></td>
                <td>Used for integrating 4 stpper drivers at maximum.</td>
            </tr>
        </tbody>
    </table>
</div>

### Pin Connection (e.g., A4988)

![pololu](./img/Arduino_Stepper_Driver_TMC2208_pololu.jpg)

![fritzing](./img/Arduino_Stepper_Driver_TMC2208_fritzing.png)

### Control Panel

#### Features

- **Stepper**
  - **RPM Speed**: sets the RPM speed of the motor (ideal range: **50-3000 rpm**, depending on the motor specification/datasheet)
  - **Direction**: sets the motor to move **clockwise** or **counterclockwise**

- **LED**
  - **Pulse Delay(ms)**: sets the blinkg speed of the LED

- **Monitor**: shows the respond after the user clicks a button or types a number

![panel.png](./img/panel.png)

#### How to design a web server with Node.js

**Learn more from [Week 14: Interface and Application Programming](https://fabacademy.wantinghsieh.com/assignment/14-interface-and-application-programming)**

#### Installation

1. Install [Node.js](https://nodejs.org/en/download/)
2. Add and upload [AdvancedFirmata](https://github.com/soundanalogous/AdvancedFirmata) to Arduino ([installation](https://github.com/soundanalogous/AdvancedFirmata#to-use))
3. Open the terminal and run the below commands in order

    ``` bash
    git clone https://github.com/cv47522/nodejs-j5-arduino-stepper.git

    cd path/to/repository

    npm i  # install the required node modules automatically

    node webServer.js  # start the server
    ```

4. Type <http://localhost:3000> in a browser to go to the client-side control panel
