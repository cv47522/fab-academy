---
visibility: true
weekId: 6
title: "Electronics Design"
publishDate: "2020-03-04"
weekImg: "06-attiny412_button_LED.png"
tags: "#KiCad #mods #PCB Milling #SMD Soldering"
assignment: "(Group) 1. Use the test equipment in your lab to observe the operation of a microcontroller circuit board. (Individual) 2. Redraw an echo hello-world board, add (at least) a button and LED (with current-limiting resistor), check the design rules, make it, and test it, extra credit: simulate its operation."
download: "https://gitlab.com/cv47522/kicad-attiny412_led_button"
---
```toc
# This code block gets replaced with the Table of Content
```

---

## Individual Assignment: PCB Design Project - ATtiny 412 Double-Sided Board

![PCB_board.png](./img/PCB_board.png)

### Materials

<div style="overflow-x:auto;">
    <table>
        <thead>
            <tr>
            <th>#</th>
            <th>Digi-Key Part No.</th>
            <th>Name</th>
            <th>Value * Pieces</th>
            <th>KiCad Footprint(Package)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row">1</th>
            <td>ATtiny 412-SSFRCT-ND</td>
            <td>ATtiny 412 w/ Single-pin UPDI (<a href='http://ww1.microchip.com/downloads/en/DeviceDoc/40001911A.pdf'>Datasheet</a>)</td>
            <td>tinyAVR™ 1 Microcontroller IC 8-Bit 20MHz 4KB FLASH 8-SOIC * 1</td>
            <td>Package_SO: SOIC-8_3.9x4.9mm_P1.27mm</td>
            </tr>
            <tr>
            <th scope="row">2</th>
            <td>B3SN-3112P</td>
            <td>B3SN Tactile Switch, SMD</td>
            <td> * 1</td>
            <td>self-designed footprint: TACTILE_SW_B3S</td>
            </tr>
            <tr>
            <th scope="row">3</th>
            <td><ul className="pl-3">
                <li>160-1889-1-ND</li>
                <li>160-1403-1-ND</li>
            </ul></td>
            <td>LED 1206 SMD</td>
            <td><ul className="pl-3">
                <li>BLUE CLEAR * 1</li>
                <li>YELLOW ORANGE * 1</li>
            </ul></td>
            <td>LED_SMD: LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder</td>
            </tr>
            <tr>
            <th scope="row">4</th>
            <td><ul className="pl-3">
                <li>311-499FRCT-ND</li>
                <li>311-10.0KFRCT-ND</li>
            </ul></td>
            <td>RES SMD 1% 1/4W 1206</td>
            <td><ul className="pl-3">
                <li>499 Ω * 2</li>
                <li>10k Ω * 1</li>
            </ul></td>
            <td>Resistor_SMD: R_1206_3216Metric_Pad1.42x1.75mm_HandSolder</td>
            </tr>
            <tr>
            <th scope="row">5</th>
            <td>445-1423-1-ND</td>
            <td>CAP CER 1UF 50V X7R 1206</td>
            <td>1 uF * 1</td>
            <td>Capacitor_SMD: C_1206_3216Metric_Pad1.42x1.75mm_HandSolder</td>
            </tr>
            <tr>
            <th scope="row">6</th>
            <td>445-1423-1-ND</td>
            <td>Male 1 Row Horizontal SMD Header 2.54MM</td>
            <td><ul className="pl-3">
                <li>Conn_01x06_Male * 1</li>
                <li>Conn_01x02_Male * 1</li>
            </ul></td>
            <td>self-designed footprint:
                <ul className="pl-3">
                <li>FTDI_Header_1x06_P2.54mm_Horizontal</li>
                <li>UPDI_Header_1x02_P2.54mm_Horizontal</li>
                </ul>
            </td>
            </tr>
        </tbody>
    </table>
</div>

### Design Tool: [KiCad EDA](https://www.kicad-pcb.org/)

KiCad EDA is a cross platform and open source electronics design automation suite with lots of predefined symbol and footprint libraries provided by industrial companies such as [Digi-Key](https://www.digikey.com/en/resources/design-tools/kicad) and [Teensy](https://github.com/XenGi/teensy_library). I used it to design my PCB by redrawing the original [hello.t412.echo board](http://academy.cba.mit.edu/classes/embedded_programming/index.html) and adding two extra LEDS (a power LED and a programmable LED) as well as a push-button.

#### Original hello.t412.echo Board

![hello.t412.echo.png](./img/hello.t412.echo.png)

#### My Board

![hello.t412.LED.Button.png](./img/hello.t412.LED.Button.png)

### Schematic Design

![Sch_mode.png](./img/Sch_mode.png)

Since I added the following components to my new board, it is common to add an extra resistor between the I/O pin of the ATtiny 412 chip or Vcc (input power) and the component itself to make sure that the current is not too high to break them:

- Power LED
- Programmable LED
- Push-Button

![hero-shot_blink.jpg](./img/hero-shot_blink.jpg)

### Decide the Current-Limiting Resistor for LED

![Sch_pwr_LED.png](./img/Sch_pwr_LED.png)

There are two main rules which I learnt from [Adafruit's Forward Voltage and KVL article](https://learn.adafruit.com/all-about-leds/forward-voltage-and-kvl) to follow in order to design the correct circuit for the LED connection:

- [Kirchhoff's Voltage Law (KVL)](https://en.wikipedia.org/wiki/Kirchhoff%27s_circuit_laws): In any **loop** of a circuit, the voltages must balance, that is **the amount generated = the amount used**

![leds_kvl.gif](./img/leds_kvl.gif#width=500px) ![leds_resistordrop.gif](./img/leds_resistordrop.gif#width=500px)

- [Ohm's Law](https://en.wikipedia.org/wiki/Ohm%27s_law): V = I * R (V: voltage, R: resistance, I: current)

First, I tried to find the forward voltage of the LED I used from its [datasheet](https://media.digikey.com/pdf/Data%20Sheets/Lite-On%20PDFs/LTST-C230TBKT.pdf) which indicates that its forward voltage (VF) and forward current (IF) are around **3V** and **20mA** respectively, which means that I need to make sure that the cross-voltage of the LED cannot exceed 3.8V or it will burn down the LED.

In other words, the resistor used to limit the current to 20mA should consume **the remaining 2V (V<sub>cc</sub> - V<sub>F</sub>, V<sub>cc</sub> =5V from USB, V<sub>F</sub>=3V)**. Then, I put these values **(V<sub>cc</sub>=5V, V<sub>F</sub>=3V, I<sub>F</sub>=20mA)** into the equation to get the correct resistance:

[[eqBlock]]
| Voltage across a resistor (V, volts) = Current through the resistor (A, amperes) * The Resistance of the resistor (Ω, ohms)<br />
| → V<sub>cc</sub> - V<sub>F</sub> = I<sub>F</sub> * R <br />
| → 5V - 3V = 0.02A (= 20mA) * R <br />
| → R = 100 Ω (the minimum value)

![LED_forward_V.png](./img/LED_forward_V.png)

According the answer, I chose 499Ω which is above the 100Ω (the minimum value) as the resistance to form the circuit for both the power LED and the programmable one.

![Sch_LEDs.png](./img/Sch_LEDs.png)

### ATtiny 412 Pins

In order to program the self-designed board, I need to understand [the function of each pin of the ATtiny 412 chip](https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/ATtiny_x12.md). Since ATtiny 412 has an Unified Program Debug Interface (UPDI) pin which is **PA0** pin, it is easier to program it with a 1x2 male pin header (**J1: Conn\_01x02\_Male**).

![ATtiny_412.gif](./img/ATtiny_412.gif)

The other 1x6 male pin header (**J2: Conn\_01x06\_Male**) is connected to a FTDI cable or [FTDI programmer board](https://gitlab.com/aaltofablab/usb-ftdi-ft230xs) to power on and read data from the ATtiny 412 board by communicating through RX (**PA1**) and TX (**PA2**) pins.

![Sch_ATtiny412.png](./img/Sch_ATtiny412.png)

For the other three remaining port pins, I used two of them as IO pins to control the programmable LED (**PA3**) and the push-button (**PA6**) respectively. Since I am not going to use **PA7** and the other two pins of the FTDI male connector, I placed a no connection flag to it to make sure it does not work against the electrical rules while I check the performance by clicking ![btn_debug.jpg](./img/btn_debug.jpg#vertical-align=middle) button.

Besides, it is also a good habit to put a [bypass capacitor](http://blog.optimumdesign.com/how-to-place-a-pcb-bypass-capacitor-6-tips) between **V<sub>cc</sub>** and **GND** pins (power pins) of the chip.

![Sch_LED_button_IO.png](./img/Sch_LED_button_IO.png)

### Annotate Schematic Symbols

In order to specify different components, I then opend the **Annotate Schematic** dialogue by clicking ![btn_annotate.png](./img/btn_annotate.png#vertical-align=middle) button to lable them with non-repetitive numbers.

![Sch_annotate.png](./img/Sch_annotate.png)

### Assign Footprint

Before layouting the printed circuit board, I need to assign which footprint belongs to which symbol (component) in the **Assign Footprint** dialogue by clicking ![btn_assign_footprint.png](./img/btn_assign_footprint.png#vertical-align=middle) button to open it.

![Sch_assign_footprint.png](./img/Sch_assign_footprint.png)

The name of each correspondent footprint is also arrranged in the [material table](#individual-pcb-design-project-attiny-412-double-sided-board) shown on top of the page.

### Generate Netlist

The final step of the schematic design is to generate the netlist file which will then be imported to the PCB layout mode. I did this by clicking ![btn_generate_netlist.png](./img/btn_generate_netlist.png#vertical-align=middle) button to open the **Generate Netlist** dialogue to generate and save the netlist file.

![Sch_generate_netlist.png](./img/Sch_generate_netlist.png)

### Complete Schematic

![Sch_board.png](./img/Sch_board.png)

### Footprint Layout

After finishing the schematic design, I then switched to the PCB layout mode via ![btn_PCB.png](./img/btn_PCB.png#vertical-align=middle) button.

![PCB_mode.png](./img/PCB_mode.png)

### Import Netlist File

In the PCB mode, I clicked ![btn_generate_netlist.png](./img/btn_generate_netlist.png#vertical-align=middle) again to load the netlist file I saved.

![PCB_load_netlist.png](./img/PCB_load_netlist.png)

These are some messy nets which I am going to layout:

![PCB_messy_net.png](./img/PCB_messy_net.png)

![PCB_after_layout.png](./img/PCB_after_layout.png)

### Define Trace Clearance, Trace Width & Via Size

Before starting to connecting components with traces, I clicked **Board setup** button ![btn_board_setup.png](./img/btn_board_setup.png#vertical-align=middle) to define the size of trace clearance, different groups of trace width and via size. I followed the rules below to set the corresponding values:

- Clearance
  - For normal traces which can be milled with **0.4mm** bits: **0.4mm**
  - For **tiny** traces (e.g., smaller SMD chips) which can only be milled with **0.3mm** bits: **0.3mm**

- Track Width
  - For **Default** traces: **0.4mm** or **0.3mm**
  - For **Power** traces (e.g., **V<sub>cc</sub>** and **GND**): **0.6mm** or more

- Via Drill: **0.8mm** (depending on the size of copper hollow rivets introduced below)

![PCB_trace_clearance.png](./img/PCB_trace_clearance.png)

#### Extra: THT (Through-hole technology) Components Setting

In order to mill THT components, I need to first adjust the hole size of certain components in the footprint library by clicking **Open footprint editor** button ![btn_footprint_editor.png](./img/btn_footprint_editor.png#vertical-align=middle). For example, here I selected a kind of headers for Li-Po batteries whose type is **JST PH S2B-PH-K**.

![PCB_footprint_library.png](./img/PCB_footprint_library.png)

![JST_PH_S2B-PH-K.jpg](./img/JST_PH_S2B-PH-K.jpg)

I double clicked its pin holes to open **Pad Properties** dialogue, typed the correspondent size (here, **1.5mm** for **Hole size X**) and made sure that the pad size of this hole is big enough for soldering by adjusting **Size X and Y**.

![PCB_Pad_Properties.png](./img/PCB_Pad_Properties.png)

### Separate Layers

After layouting all the components, I started to create traces between them by using ![btn_add_wires.png](./img/btn_add_wires.png#vertical-align=middle) or typing its shortcut **W**.

If there is not enough space for routing tracks, I then created [vias](https://en.wikipedia.org/wiki/Via_(electronics)) which are conductive holes by using ![btn_add_vias.png](./img/btn_add_vias.png#vertical-align=middle) or typing its shortcut **V** to make traces first go to the other side then come back to the original side for its connection:

![PCB_add_wires.png](./img/PCB_add_wires.png)

![PCB_add_vias.png](./img/PCB_add_vias.png)

Since there are many pins connect to **GND**, it is also useful to create a ground plane on the other side by switching to **B.Cu** layer and using ![btn_add_fill_zone.png](./img/btn_add_fill_zone.png#vertical-align=middle) **Add fill zones** tool to first frame the area then assign it as a **GND plane**.

![PCB_copper_zone.png](./img/PCB_copper_zone.png)

![PCB_add_fill_zone.png](./img/PCB_add_fill_zone.png)

The final step of the PCB layout is to define its outline for [mods](http://mods.cba.mit.edu/) to generate its interior toolpath. To do that, I first switched to the **Edge.Cuts** layer and used ![btn_add_lines.png](./img/btn_add_lines.png#vertical-align=middle) **Add graphic lines** tool to draw a rectangle area and then switched to the **Dwgs.User** layer to fill the field with ![btn_add_polygon.png](./img/btn_add_polygon.png#vertical-align=middle) **Add graphic polygon** tool.

![PCB_layer_Edge.Cuts.png](./img/PCB_layer_Edge.Cuts.png)

![PCB_layer_Dwgs.User.png](./img/PCB_layer_Dwgs.User.png)

Finally, I added a **pink** rectangular outline to the **Margin** layer which is reserved for the drawing of circuit board outline. Any element (graphic, texts…) placed on this layer appears on all the other layers. It also defines the **board area** for exporting the image for each layer.

### Complete PCB View & Final Check

Below I listed some notes according to different layers:

- **Edge.Cuts** layer: **yellow** outline
  - every component (including gray texts and green footprints) should be inside this layer.

- **Dwgs.User** layer: **gray** fill
  - the size of this layer should be the same as **Edge.Cuts** layer.

- **Margin** layer: **pink** outline
  - make sure there is no **gray texts** in this layer, otherwise there will be some flipping problems during milling.
  - make sure the left and right margins are **even** by specifically defining the coordinates of two vertical lines along x-axis in **Line Segment Properties** dialogue.

![PCB_Line_Segment_Properties.png](./img/PCB_Line_Segment_Properties.png)

![PCB_board.png](./img/PCB_board.png)

### Export PCB SVG

Before starting milling the board, we need to generate the image files for its **front-side traces**, **back-side traces**, **drilling** and **outline**. I did this with the following steps and settings:

![PCB_export_svg.png](./img/PCB_export_svg.png)

![PCB_export_F.Cu.png](./img/PCB_export_F.Cu.png)

![PCB_export_B.Cu.png](./img/PCB_export_B.Cu.png)

For the **drilling svg**, I filtered out the vias from the **F.Cu.svg** and infilled them with black color in [Inkscape](https://inkscape.org/):

![Inkscape_fill_color.png](./img/Inkscape_fill_color.png)

### Fabrication: SRM-20 Milling Machine

The fabrication of the PCB is almost the same as the process of [Week 4: Electronics Production](https://fabacademy.wantinghsieh.com/assignment/04-electronics-production) except the server module used to generate the cutting toolpath in [mods](http://mods.cba.mit.edu/) is:

p.s. **MDX-40** milling machine uses the follwing same svg mods server module.

![mods_PCB_svg.png](./img/mods_PCB_svg.png)

#### mods Toolpath Setting Comparison (MDX-40)

##### [Link to tutorial of MDX-40 milling machine](https://youtu.be/dmjhjA_-xRc)

- origin: (x, y, z) = **(0, 0, 0) mm**
- home: (x, y, z) = **(0, 0, 5) mm**

<div style="overflow-x:auto;">
    <table>
        <thead>
            <tr>
                <th>tool diameter</th>
                <th>cut depth</th>
                <th>max depth</th>
                <th>offset number</th>
                <th>speed</th>
                <th>color</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">0.3mm</th>
                <td>0.125mm</td>
                <td>0.125mm</td>
                <td>4</td>
                <td>1.0mm/s</td>
                <td>
                    <ul>
                        <li>Traces: white</li>
                        <li>Background: black</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <th scope="row">0.4mm</th>
                <td>0.125mm</td>
                <td>0.125mm</td>
                <td>4</td>
                <td>1.5mm/s</td>
                <td>
                    <ul>
                        <li>Traces: white</li>
                        <li>Background: black</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <th scope="row">0.8mm (Outline)</th>
                <td>0.6mm</td>
                <td>1.8mm</td>
                <td>1</td>
                <td>2.0mm/s</td>
                <td>
                    <ul>
                        <li>Inside: white</li>
                        <li>Outside: black</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <th scope="row">0.73mm (Drill)</th>
                <td>0.6mm</td>
                <td>1.8mm</td>
                <td>1</td>
                <td>2.0mm/s</td>
                <td>
                    <ul>
                        <li>Holes: <strong>black</strong></li>
                        <li>Background: white</li>
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>
</div>

Front Side: [F_Cu.svg](https://gitlab.com/cv47522/kicad-attiny412_led_button/-/blob/master/svg/F_Cu.svg) (0.3mm), [F_Drills.svg](https://gitlab.com/cv47522/kicad-attiny412_led_button/-/blob/master/svg/F_Drills.svg) & [Dwgs_User.svg](https://gitlab.com/cv47522/kicad-attiny412_led_button/-/blob/master/svg/Dwgs_User.svg) (0.8mm)

![F_cu.jpg](./img/F_cu.jpg)

Back Side: [B_Cu.svg](https://gitlab.com/cv47522/kicad-attiny412_led_button/-/blob/master/svg/B_Cu.svg) (0.4mm)

![B_cu.jpg](./img/B_cu.jpg)

### Fill Holes with [Vias](https://en.wikipedia.org/wiki/Via_(electronics)) to Connect Both Sides

Before soldering the PCB, I first inserted the **copper hollow rivets** ![rivets.jpg](./img/rivets.jpg) to make traditional vias by using a hammer and its flanging tools which are a [marking punch](https://www.mcmaster.com/3498a11) and a [pin-loosening punch](https://www.mcmaster.com/3415a13).

![vias_rivets.png](./img/vias_rivets.png#width=400px) ![marking_punch.png](./img/marking_punch.png#width=300px) ![pin-loosening_punch.png](./img/pin-loosening_punch.png#width=300px)

### Solder PCB with [Flux](https://en.wikipedia.org/wiki/Flux_(metallurgy))

It is also beneficial to use a liquid flux pen to put some flux on the board before soldering any SMD components.

![solder_flux.png](./img/solder_flux.png)

### End Result

The board I redrew finally works after I programmed it. The ATtiny 412 programming process and the setup environment will be introduced in [Week 8: Embedded Programming](https://fabacademy.wantinghsieh.com/assignment/06-electronics-design).

#### Blink the LED

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/ATtiny412_blink-small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/ATtiny412_button-small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

### Problem & Solution

The inital connection of the switch I designed was wrong, which I found out after uploading the code to the board. It lacked a **10kΩ resistor** as a pull-up or pull-down resistor whose function can be found from the Sparkfun: [Pull-up Resistors](https://learn.sparkfun.com/tutorials/pull-up-resistors/all) article. Since it takes quite a long time to reconstruct a new board, I then decided to solder a through-hole resistor to fix the problem.

![Sch_SW_wrong.png](./img/Sch_SW_wrong.png)

![hero-shot_no_R.jpg](./img/hero-shot_no_R.jpg)

![Sch_SW_correct.png](./img/Sch_SW_correct.png)

![hero-shot_add_R.jpg](./img/hero-shot_add_R.jpg)

It works after I reuploaded the code.

#### LED OFF

![hero-shot_btn.jpg](./img/hero-shot_btn.jpg)

#### Press the button to light up LED

![hero-shot_press_btn.jpg](./img/hero-shot_press_btn.jpg)

---

## Group Assignment: Debugging & Measurement

### [Link to Documentation Page](https://aaltofablab.gitlab.io/fab-academy-2020/electronics-design/)

### Debugging Tool: [Multimeter](https://learn.sparkfun.com/tutorials/how-to-use-a-multimeter/all)

After finishing soldering, I used a multimeter and switched it to the **buzzing mode** which sounds if two points are connected (**0Ω** between them). In order to find out how much current does each LED consume, I then measured their cross voltage by switching to the **DC voltage** mode:

#### Measure the Connection

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/PCB_measure_R-small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

#### Measure the Cross Voltage

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/PCB_measure_V-small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

The red dots between each component are multimeter probes. According to the statistic, I can calculate the exact current which go through both the LED and the resistor:

[[eqBlock]]
| Voltage across a resistor (V, volts) = Current through the resistor (A, amperes) * The Resistance of the resistor (Ω, ohms) <br />
| → V<sub>cc</sub> - V<sub>F</sub> = I<sub>F</sub> * R <br />
| → V<sub>R</sub> = I<sub>F</sub> * R <br />
| → 2.148V = I<sub>F</sub> * 499 Ω <br />
| → I<sub>F</sub> = 0.0043A = 4.3mA

#### V<sub>F</sub>: Cross Voltage of Power LED

![Sch_pwr_LED_V_measure.png](./img/Sch_pwr_LED_V_measure.png)

![PCB_measure_pwr_LED_V.png](./img/PCB_measure_pwr_LED_V.png)

#### V<sub>R</sub>: Cross Voltage of Resistor

![Sch_pwr_LED_R_V_measure.png](./img/Sch_pwr_LED_R_V_measure.png)

![PCB_measure_pwr_LED_R_V.png](./img/PCB_measure_pwr_LED_R_V.png)

Then I used the multimeter to verify my calculation and it seems that there is some difference between the theoretical number and the practical one when it comes to measuring small current which can be observed by an [oscilloscope](https://learn.sparkfun.com/tutorials/how-to-use-an-oscilloscope/all) in a more precise way:

#### LED OFF

![PCB_measure_pwr_LED_I_off.jpg](./img/PCB_measure_pwr_LED_I_off.jpg)

#### LED ON

![PCB_measure_pwr_LED_I_off.jpg](./img/PCB_measure_pwr_LED_I_on.jpg)