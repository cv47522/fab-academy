---
visibility: true
weekId: 10
title: "Applications and Implications"
publishDate: "2020-04-01"
weekImg: "10-flower.jpg"
tags: "#Final Project Proposal"
assignment: "Propose a final project masterpiece that integrates the range of units covered. Your project should incorporate:

1. 2D and 3D design
2. Additive and subtractive fabrication processes
3. Electronics design and production
4. Microcontroller interfacing and programming
5. System integration and packaging

Where possible, you should make rather than buy the parts of your project. Projects can be separate or joint, but need to show individual mastery of the skills, and be independently operable."
download: ""
---
```toc
# This code block gets replaced with the Table of Content
```

---

## [Link to Final Project Documentation Page](https://fabacademy.wantinghsieh.com/assignment/final)

---

## Final Project Proposal

In this proposal, presents one work, “Breath of Flora” —— an interactive sound and light mechanical flower. Infrared sensors and sound detection sensors are used in the artwork as the main technique to interact with people. The work adopts “Listening” as the concept and is going to be displayed in the oldest and longest functioning mental hospital in Finland —— Lapinlahti Hospital around summer in 2020.

### Keywords

Art Education, Physical Perception, Kinetic Sculpture, Distance and Sound Detection, Interactive Installation

### What will it do?

The artwork will be installed on the corridor wall in the hospital as a decoration to intrigue passersby to stop and interact with it. Motors, gears, metal sheets, and microcontrollers are implemented to build the work into a mechanism in a shape of a flower for the purpose of echoing to one of the most essential medical treatments to patients —— the outside large blooming garden. Since natural surrounding plays an advantageous role in the cure of patients, it was taken into account while constructing the hospital, which was in fact unconventional in those days.

In the dim quiet passage full of kinetic flowers, these blossoms act as if they were transformed from consciousness of the past sufferers living with perception, leading audiences to listen and feel the patients' apprehension and belongingness deep inside their hearts. By detecting the distance between pedestrians and the installation with an infrared sensor, the inner microcontroller sends a signal to trigger the flower to blossom and light up LEDs hiding inside petals at the breathing rate. On the other hand, the script of a soliloquizing speaker placed inside the ovary of the flower installation correlates with the volume of the sound in the space which is observed by a sound detection sensor.

![space.jpg](./img/space.jpg)

### Who has done what beforehand?

I found a final project about [the mechanical blooming flower](http://archive.fabacademy.org/archives/2016/fablabaachen/students/197/projects/final_project.html) from the 2016 fab academy archive. The author used a servo as its output device, 3D printed components and poplar wood to construct the hardware parts. For the interaction, he adopted a photoresistor as its sensor which requires close/touchable detection.

![archive_flower.jpg](./img/archive_flower.jpg)

### What will you design?

The proposal mainly focuses on how to augment new media art to intervene in our daily life with the approach of enhancing the interaction between viewers and artworks. Most art pieces in domestic hospitals such as paintings, sculptures and calligraphy are all in the static state and passively waiting for people to appreciate and discover their notions. “Breath of Flora” —— an interactive sound and light mechanical flower contributes a whole new experience of art appreciation in the following aspects:

1. By combining art with technology, it actively educates audiences to put themselves in patients’ shoes and introspect whether if they have the right attitude to hearken to sick people, which enhances the connection between new media art and psychiatric hospitals.

2. Compared to still sculptures decorated in the majority of public space, the kinetic installation is made from electronic components and programmed with codes for the sake of returning sound and light feedback depending on the length and noise level during the interaction.

3. Since the way of how every passerby interacts with the artwork is unique, the installation has also become a kind of participatory art which engages public participation in the innovative process, letting them become co-authors, editors, and observers of the work and is deficient without audiences' physical interaction, making it into an exclusive artwork ultimately.

### What materials and components will be used?

<div style="overflow-x:auto;">
    <table>
        <thead>
          <tr>
            <th>#</th>
            <th>Digi-Key Part No.</th>
            <th>Name</th>
            <th>Value/Size</th>
            <th>Price * Pieces</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>-</td>
            <td>Stepper Motor</td>
            <td>NEMA 11</td>
            <td>€10-20 * 5</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>ATTINY1614-SSNRTR-ND</td>
            <td>ATtiny1614</td>
            <td>IC MCU 8BIT 16KB FLASH 14SOIC</td>
            <td>€0.6 * 10</td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>-</td>
            <td>Power Adapter</td>
            <td>9V</td>
            <td>€10-15 * 5</td>
          </tr>
          <tr>
            <th scope="row">4</th>
            <td>CP-43515RSSJTR-ND</td>
            <td>Headphone Phone Jack Stereo</td>
            <td>3.5MM</td>
            <td>€0.6 * 5</td>
          </tr>
          <tr>
            <th scope="row">5</th>
            <td>CP1-2533SJCT-ND</td>
            <td>CONN-POWER JACK SMD</td>
            <td>2.1MM</td>
            <td>€1 * 5</td>
          </tr>
          <tr>
            <th scope="row">6</th>
            <td>458-1124-ND‎</td>
            <td>SPEAKER</td>
            <td>8OHM 250MW</td>
            <td>€0.7 * 5</td>
          </tr>
          <tr>
            <th scope="row">7</th>
            <td>-‎</td>
            <td>Amplifier</td>
            <td>-</td>
            <td>€1 * 5</td>
          </tr>
          <tr>
            <th scope="row">8</th>
            <td>SPU0414HR5H-SB-7</td>
            <td>Analog Microphone MEMS	</td>
            <td>100Hz-10kHz</td>
            <td>€1.5 * 5</td>
          </tr>
          <tr>
            <th scope="row">9</th>
            <td>620-1428-1-ND</td>
            <td>IC MOTOR DRIVER</td>
            <td>8V-40V 8SOIC</td>
            <td>€1.5 * 5</td>
          </tr>
          <tr>
            <th scope="row">10</th>
            <td>LM3480IM3-3.3/NOPBCT-ND</td>
            <td>Description	Linear Voltage Regulator</td>
            <td>3V,5V Output 100mA SOT-23-3</td>
            <td>€1 * 10</td>
          </tr>
          <tr>
            <th scope="row">11</th>
            <td>1080-1380-1-ND</td>
            <td>Phototransistor</td>
            <td>940nm</td>
            <td>€0.4 * 5</td>
          </tr>
          <tr>
            <th scope="row">12</th>
            <td>-</td>
            <td>Diode SMD</td>
            <td>-</td>
            <td>€1 * 10</td>
          </tr>
          <tr>
            <th scope="row">13</th>
            <td>-</td>
            <td>Capacitor SMD</td>
            <td>-</td>
            <td>€0.3 * 30</td>
          </tr>
          <tr>
            <th scope="row">14</th>
            <td>160-1889-1-ND</td>
            <td>LED SMD</td>
            <td>Single Color</td>
            <td>€0.5 * 20</td>
          </tr>
          <tr>
            <th scope="row">15</th>
            <td>CLV1A-FKB-CK1VW1DE1BB7C3C3CT-ND</td>
            <td>LED SMD</td>
            <td>RGB</td>
            <td>€0.4 * 30</td>
          </tr>
          <tr>
            <th scope="row">16</th>
            <td>-</td>
            <td>Resistor SMD</td>
            <td>-</td>
            <td>€0.1 * 50</td>
          </tr>
          <tr>
            <th scope="row">17</th>
            <td>NDS355AN</td>
            <td>MOSFET N-Channel</td>
            <td>30V 1.7A SSOT3</td>
            <td>€0.1 * 10</td>
          </tr>
          <tr>
            <th scope="row">18</th>
            <td>-</td>
            <td>3D Printer Filament</td>
            <td>PLA 2-3kg</td>
            <td>€20 * 3</td>
          </tr>
          <tr>
            <th scope="row">19</th>
            <td>-</td>
            <td>Gears, Shafts, Collars, Flanged Bearings, Linkages, Screws...</td>
            <td>-</td>
            <td>€10 * 5</td>
          </tr>
          <tr>
            <th scope="row">20</th>
            <td>-</td>
            <td>Brass wires/tubes</td>
            <td>5M</td>
            <td>€10 * 5</td>
          </tr>
          <tr>
            <th scope="row">21</th>
            <td>-</td>
            <td>Consumables</td>
            <td>-</td>
            <td>€5</td>
          </tr>
        </tbody>
    </table>
</div>

### Where will they come from?

Most of the materials used for my final project will be bought from [DigiKey](https://www.digikey.com/). Others will be found from hardware stores, online shops or [Aalto Fablab](https://fablab.aalto.fi/).

### How much will they cost?

According to the material table above, the total amount is around **€500** for **5** units (i.e., **€100** per flower).

### What parts and systems will be made?

The “Breath of Flora” —— interactive sound and light mechanical flowers will be divided into these parts during its development:

- **2D and 3D design**  
  Before fabricating the hardware parts, I will use [Fusion 360](https://www.autodesk.com/products/fusion-360/overview) to model its structures, design its petals and simulate its blossom movement.

- **Additive and subtractive fabrication processes**  
  3D printers such as [Ultimaker 2+ Extended](https://ultimaker.com/3d-printers/ultimaker-2-plus) will be used as tools for the addictive fabrication process to manufacture its 3D components designed in Fusion 360.

  A laser cutter or a CNC milling machine will be adopted to subtractively fabricate a wooden box for packaging all the electrical components.

- **Electronics design and production**  
  I will use [KiCad](https://www.kicad-pcb.org/) for designing its electrical circuits and [SRM-20](https://www.rolanddga.com/products/3d/srm-20-small-milling-machine/features) or [MDX-40](https://youtu.be/dmjhjA_-xRc) for milling its printed circuit board.

- Microcontroller interfacing and programming  
  The [megaTinyCore Arduino library](https://github.com/SpenceKonde/megaTinyCore) will be used for programming the board to trigger a stepper motor to open the flower, light up the LEDs installed inside its ovary as well as play pre-recorded human voice sound files saved in a SD card.

  Since I will use [ATtiny 1614](https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/ATtiny_x14.md) which is a newer series of AVR microcontrollers for my final project, the [Python UPDI library](https://github.com/mraardvark/pyupdi) can be used for uploading the compiled hex file to it through its UPDI pin.

![sound_on.jpg](./img/sound_on.jpg)

![sound_off.jpg](./img/sound_off.jpg)

- **System integration and packaging**  
  The kinetic mechanism including gears, linkages and rods will also be integrated with electrcal parts for making its blossom movement.

![close.jpg](./img/close.jpg)

![open.jpg](./img/open.jpg)

### What processes will be used?

After finishing making the systems above, there are some other processes left:

- **Sanding/Polishing**  
  Because of the shell thickness set in [Cura](https://ultimaker.com/software/ultimaker-cura), some 3D printed components need to be sanded to keep their precision before assembling them together.

- **Painting**  
  In order to make the appearance of my final project more realistic or fantastic, I might need to spray or paint it to cover its raw pattern.

- **Assembling**  
  After testing each component, I might need to assemble and package all of the electrical parts within a fabricated wooden box to beautify its appearance.

- **Wires organizing**  
  Organizing electrical wires is also important for speeding up future debugging.

### What questions need to be answered?

So far there are some questions I might need to think about at the moment:

- What is the major difference between using a Teensy plus its audio shield board and own customized microcontroller for my final project?

- Is there any other materials which are lighter and firm than PLA filaments while fabricating the petals?

- How many currents will be consumed by each flower unit?

- How to make a module that can save sound files inside a SD card?

- How to install the flower on a wall without affecting the stabilization of its blossom movement?

- How to make each flower unit lighter and easier to install so that it can be conveniently carried around?

### How will it be evaluated?

For my own evaluation, the first flower prototype should follow its program to interact with audiences in real-time without any delay or shutdown. It is also important to me to get the feedback about whether or not the interaction is intuitive without any guidance from the people who play with it.
