---
visibility: true
weekId: 4
title: "Electronics Production"
publishDate: "2020-02-19"
weekImg: "04-UPDI_hero-shot.jpg"
tags: "#mods #PCB Milling #SMD Soldering"
assignment: "(Group) 1. Characterise the design rules for your PCB production process: document feeds, speeds, plunge rate, depth of cut (traces and outline) and tooling. (Individual) 2. Document your work. 3. Make an in-circuit programmer by milling and stuffing the PCB, test it, then optionally try other PCB fabrication process."
download: "https://gitlab.com/cv47522/fab-academy/-/tree/master/src/posts/04-electronics-production/source_files"
---
```toc
# This code block gets replaced with the Table of Content
```

---

## Individual Assignment: In-circuit Programmer PCB Milling & Stuffing

![hello.USB-UPDI.FT230X-functional.jpg](./img/hello.USB-UPDI.FT230X-functional.jpg)

### Materials

<div style="overflow-x:auto;">
    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">1</th>
                <td>3D Milling Machine: Roland SRM-20 (<a href='http://support.rolanddga.com/docs/documents/departments/technical%20services/manuals%20and%20guides/srm-20_use_en.pdf'>Manual</a>)</td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>Soldering Iron</td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>0.3mm, 0.4mm, 0.8mm PCB Milling Bits</td>
            </tr>
            <tr>
                <th scope="row">4</th>
                <td>Single Sided Copper Clad Laminate</td>
            </tr>
            </tbody>
    </table>
</div>

### Setup: Convert PNG Circuit Images to Bit Toolpaths in [mods](http://mods.cba.mit.edu/)

I used [hello.serial-UPDI.FT230X](http://academy.cba.mit.edu/classes/embedded_programming/index.html#programmers) and [hello.USB-UPDI.FT230X](http://academy.cba.mit.edu/classes/embedded_programming/index.html#programmers) file for my PCB milling fabrication. For milling the traces, we are going to use **0.4mm** diameter bit for the former one and **0.3mm** for the latter one to remove some of the top copper and then form the final circuit. The bit with **0.8mm** diameter is used to cut off the interior (outline) of both boards. Each bit with different diameter has its own suitable cutting speed:

- 0.3mm bit: tiny traces
- 0.4mm bit: normal traces
- 0.8mm bit: interior (outline)

![pcb-cutting-speed.jpg](./img/pcb-cutting-speed.jpg)

![bits.jpg](./img/bits.jpg)

[hello.serial-UPDI.FT230X](http://academy.cba.mit.edu/classes/embedded_programming/index.html#programmers): 0.4mm (traces), 0.8mm (interior)

![0.4+0.8mm.jpg](./img/0.4+0.8mm.jpg)

[hello.USB-UPDI.FT230X](http://academy.cba.mit.edu/classes/embedded_programming/index.html#programmers): 0.3mm (traces), 0.8mm (interior)

![0.3+0.8mm.jpg](./img/0.3+0.8mm.jpg)

### mods Toolpath Setting Comparison (SRM-20)

#### [Link to tutorial of MDX-40 milling machine](https://youtu.be/dmjhjA_-xRc)

- origin: (x, y, z) = **(0, 0, 0) mm**
- home: (x, y, z) = **(0, 0, 5) mm**

<div style="overflow-x:auto;">
    <table>
        <thead>
            <tr>
                <th>tool diameter</th>
                <th>cut depth</th>
                <th>max depth</th>
                <th>offset number</th>
                <th>speed</th>
                <th>file</th>
                <th>color</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">0.3mm</th>
                <td>0.1mm</td>
                <td>0.1mm</td>
                <td>4</td>
                <td>1.0mm/s</td>
                <td><a href='http://academy.cba.mit.edu/classes/embedded_programming/FTDI/USB-FT230XS-UPDI.traces.png'>USB-FT230XS-UPDI.traces.png</a></td>
                <td>
                    <ul>
                        <li>Traces: white</li>
                        <li>Background: black</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <th scope="row">0.4mm</th>
                <td>0.1mm</td>
                <td>0.1mm</td>
                <td>4</td>
                <td>1.5mm/s</td>
                <td><a href='http://academy.cba.mit.edu/classes/embedded_programming/UPDI/FTDI-UPDI.traces.png'>FTDI-UPDI.traces.png</a></td>
                <td>
                    <ul>
                        <li>Traces: white</li>
                        <li>Background: black</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <th scope="row">0.8mm (Outline)</th>
                <td>0.6mm</td>
                <td>1.8mm</td>
                <td>1</td>
                <td>2.0mm/s</td>
                <td>
                    <ul className='pl-3'>
                        <li><a href='http://academy.cba.mit.edu/classes/embedded_programming/UPDI/FTDI-UPDI.interior.png'>FTDI-UPDI.interior.png</a></li>
                        <li><a href='http://academy.cba.mit.edu/classes/embedded_programming/FTDI/USB-FT230XS-UPDI.interior.png'>USB-FT230XS-UPDI.interior.png</a></li>
                    </ul>
                </td>
                <td>
                    <ul>
                        <li>Inside: white</li>
                        <li>Outside: black</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <th scope="row">0.73mm (Drill)</th>
                <td>0.6mm</td>
                <td>1.8mm</td>
                <td>1</td>
                <td>2.0mm/s</td>
                <td></td>
                <td>
                    <ul>
                        <li>Holes: <strong>black</strong></li>
                        <li>Background: white</li>
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>
</div>

### Generate Trace Toolpath
In order to generate the toolpath, I went to the [mods](http://mods.cba.mit.edu/) website and created the **SRM-20 PCB png milling program**.

![mods-1.jpg](./img/mods-1.jpg)

There are total two png files (trace and interior) which form the final circuit. I first uploaded the trace file to mods. Then I did the following steps in order:

1. click **mill traces (1/64)** button
2. input parameters in the mill raster 2D module
3. input the correspondent cutting speed and set the origin to zero
4. delete the final WebSocket device module and create a new **save file** module
5. click the **calculate** button in the mill raster 2D module

#### 0.4mm Bit Setting ([FTDI-UPDI.traces.png](http://academy.cba.mit.edu/classes/embedded_programming/UPDI/FTDI-UPDI.traces.png))

![mods_0.4_setting.png](./img/mods_0.4_setting.png)

#### 0.3mm Bit Setting ([USB-FT230XS-UPDI.traces.png](http://academy.cba.mit.edu/classes/embedded_programming/FTDI/USB-FT230XS-UPDI.traces.png))

![mods_0.3_setting.png](./img/mods_0.3_setting.png)

##### The generated trace toolpath

![mods_traces.png](./img/mods_traces.png)

#### Generate Interior Toolpath: 0.8mm Bit ([USB-FT230XS-UPDI.interior.png](http://academy.cba.mit.edu/classes/embedded_programming/FTDI/USB-FT230XS-UPDI.interior.png), [FTDI-UPDI.interior.png](http://academy.cba.mit.edu/classes/embedded_programming/UPDI/FTDI-UPDI.interior.png))

After generating the trace file, I then uploaded the interior image to mods with the same steps but different parameters to save the outline cutting toolpath file.

![mods_0.8_setting.png](./img/mods_0.8_setting.png)

### Fabrication: SRM-20 Milling Machine

After downloading the **rml** trace and interior toolpath file, I installed the correspondent bit and turned to the SRM-20 control panel to set up the cutting preference:

![SRM_20_step6_moveBit.jpg](./img/SRM_20_step6_moveBit.jpg)

I moved z-axis to reach its lowest point by clicking the downward arrow in first **Continue** steps then **x100** tiny steps. After that, I set the z-axis to zero.

![SRM_20_step1_z_reachLowestPoint.png](./img/SRM_20_step1_z_reachLowestPoint.png)

![SRM_20_step2_z_setZero.png](./img/SRM_20_step2_z_setZero.png)

Then I lifted up the z-axis for **2mm** and set it to zero again as well as both x-axis and y-axis.

![SRM_20_step3_z_liftUp2mm.png](./img/SRM_20_step3_z_liftUp2mm.png)

![SRM_20_step4_z_setZeroAgain.png](./img/SRM_20_step4_z_setZeroAgain.png)

![SRM_20_step5_xy_setZero.png](./img/SRM_20_step5_xy_setZero.png)

I turned to the milling machince and released the screw to make the bit lower enough to touch the top of the board.

![SRM_20_step6_moveBit.jpg](./img/SRM_20_step6_moveBit.jpg)

![SRM_20_step7_lowerBit.jpg](./img/SRM_20_step7_lowerBit.jpg)

Before starting cutting, I lifted up the z-axis again for 2mm, slowed down the initial cutting and spindle speed and then pressed **Cut** button. In order to make the job queue clean, I first **Delete All** jobs and then **Add** the new one. After that, I clicked **Output** to cut the traces or the outline.

![SRM_20_step9_slowDownSpeed.png](./img/SRM_20_step9_slowDownSpeed.png)

![SRM_20_step10_loadCuttingFile.png](./img/SRM_20_step10_loadCuttingFile.png)

If there is not any problem with the initial cutting, I then adjusted the values of both speeds to normal.

![SRM_20_step11_speedUp.png](./img/SRM_20_step11_speedUp.png)

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/USB-FT230XS-UPDI.traces-small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

#### hello.serial-UPDI.FT230X

![hello.serial-UPDI.FT230X-1.jpg](./img/hello.serial-UPDI.FT230X-1.jpg)

#### hello.USB-UPDI.FT230X

![hello.USB-UPDI.FT230X-1.jpg](./img/hello.USB-UPDI.FT230X-1.jpg)

### Assembling & Soldering

#### Electronic Components

<div style="overflow-x:auto;">
    <table>
        <thead>
            <tr>
            <th>board</th>
            <th>SMD IC</th>
            <th>SMD resistor</th>
            <th>SMD capacitor</th>
            <th>SMD header</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <td><a href='http://academy.cba.mit.edu/classes/embedded_programming/UPDI/FTDI-UPDI.png'>hello.serial-UPDI.FT230X</a></td>
            <td>-</td>
            <td>4.99k Ω * 1</td>
            <td>-</td>
            <td>
                <ul className='pl-3'>
                <li>2.54mm male 1 row horizon header * 1</li>
                <li>2W 2.54mm PTH SOCKET SIL SMT * 1</li>
            </ul>
            </td>
            </tr>
            <tr>
            <td><a href='http://academy.cba.mit.edu/classes/embedded_programming/FTDI/USB-FT230XS-UPDI.png'>hello.USB-UPDI.FT230X</a></td>
            <td>FT230XS</td>
            <td>
                <ul className='pl-3'>
                <li>4.99k Ω * 1</li>
                <li>49 Ω * 2</li>
                </ul>
            </td>
            <td>
                <ul className='pl-3'>
                <li>1 uF * 1</li>
                <li>10 pF * 2</li>
                </ul>
            </td>
            <td>2W 2.54mm PTH SOCKET SIL SMT * 1</td>
            </tr>
        </tbody>
    </table>
</div>

It is harder to solder SMD components than the DIP ones. Therefore, I used a magnifier to make sure that I can clearly see each pin of the components while soldering the bords especially the SMD IC.

![magnifier.jpg](./img/magnifier.jpg)

#### Hero Shot: hello.serial-UPDI.FT230X

![hello.serial-UPDI.FT230X-2.jpg](./img/hello.serial-UPDI.FT230X-2.jpg)

![hello.serial-UPDI.FT230X-3.jpg](./img/hello.serial-UPDI.FT230X-3.jpg)

#### Hero Shot: hello.USB-UPDI.FT230X

![hello.USB-UPDI.FT230X-2.jpg](./img/hello.USB-UPDI.FT230X-2.jpg)

![hello.USB-UPDI.FT230X-3.jpg](./img/hello.USB-UPDI.FT230X-3.jpg)

### Problem & Solution

I spent a lot of time on adjusting the origin of the z-axis of the SRM-20 milling machine since it didn't actually cut through the copper layer but lifted up after pessing the **Output** button at the beginning. Then I found out that the problem is that the origin value of the **Roland SRM-20 milling machine** module which I didn't change but left it as default values (10, 10, 10) on the [mods](http://mods.cba.mit.edu/) website had made the machine lift up for 10mm during the cutting process.

![problem-1.png](./img/problem-1.png)

### Program the Board

I programmed the [Hello ATtiny1614 Blink board](https://gitlab.com/aaltofablab/hello-attiny1614-blink) with the hello.USB-UPDI.FT230X I made by uploading the Arduino script through the command lines in the terminal. Finally, my board works!

![hello.USB-UPDI.FT230X-functional.jpg](./img/hello.USB-UPDI.FT230X-functional.jpg)

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/hello.USB-UPDI.FT230X-functional-small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
