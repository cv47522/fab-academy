---
visibility: true
weekId: 8
title: "Embedded Programming"
publishDate: "2020-03-18"
weekImg: "08-blink_board.jpg"
tags: "#Python #Arduino IDE #pyUPDI #megaTinyCore"
assignment: "(Group) 1. Compare the performance and development workflows for different microcontroller families. (Individual) 2. Read the datasheet for the microcontroller you are programming. 3. Program the board you have made to do something, with as many different programming languages and programming environments as possible."
download: "https://gitlab.com/cv47522/kicad-attiny412_led_button"
---
```toc
# This code block gets replaced with the Table of Content
```

---

## Individual Assignment: ATtiny 412 Double-Sided Board Programming

![board_ports.jpg](./img/board_ports.jpg)

### Materials

<div style='overflow-x:auto;'>
    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Source</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>ATtiny 412 Double-Sided Board</td>
                <td>
                    <a href="/assignment/06-electronics-design" target='_blank'>Week 6: Electronics Design</a>
                </td>
                <td>The main board for programming.</td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>USB FTDI FT230XS Host Board</td>
                <td>
                    <a href="https://gitlab.com/aaltofablab/usb-ftdi-ft230xs" target='_blank'>
                        Aalto Fablab
                    </a>
                </td>
                <td>
                    Used for replacing a <a href="https://www.ftdichip.com/Products/Cables/USBTTLSerial.htm" target='_blank'>
                        FTDI cable
                    </a>.
                </td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>USB UPDI FT230XS Programmer</td>
                <td>
                    <a href="https://gitlab.com/aaltofablab/usb-updi-ft230xs" target='_blank'>
                        Aalto Fablab
                    </a>
                </td>
                <td>
                    Used as a 2-pin programmer board for uploading the code to the main
                    ATtiny 412 board through it.
                </td>
            </tr>
            <tr>
                <th scope="row">4</th>
                <td>USB Extension Cable</td>
                <td>
                    <a href="https://www.amazon.com/AmazonBasics-Extension-Cable-Male-Female/dp/B00NH11R3I" target='_blank'>
                        Amazon
                    </a>
                </td>
                <td>Used for connecting to the USB UPDI FT230XS Programmer.</td>
            </tr>
            <tr>
                <th scope="row">5</th>
                <td>USB Hub</td>
                <td>
                    <a href="https://www.amazon.com/-/zh_TW/dp/B00L2442H0/ref=sr_1_3?dchild=1&keywords=USB+hub&qid=1587914225&s=electronics&sr=1-3" target='_blank'>
                        Amazon
                    </a>
                </td>
                <td>-</td>
            </tr>
        </tbody>
    </table>
</div>

### Read a Microcontroller Datasheet

From the [ATtiny 412 datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/40001911A.pdf), I learnt that each pin has different functions (e.g., in addtion to normal I/O, **PA1** and **PA2** can also be used as **TX** and **RX** for serial communication with a computer. **PA0** can be connected to a **UPDI** programmer for uploading a hex file.) With the code below, the ATtiny 412 board can read the input text from **Arduino Serial Monior** through a **FTDI** cable or host board withouting connecting any other I/O devices.

![echo1.png](./img/echo1.png)

![echo2.png](./img/echo2.png)

#### [ATtiny412_echo.ino](https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_echo)

```arduino
#define max_buffer 25

static int index = 0;
static char chr;
static char buffer[max_buffer] = {0};

void setup() {
   Serial.begin(115200);
}

void loop() {
   if (Serial.available() > 0) {
      chr = Serial.read();
      Serial.print("hello.t412.echo: you typed \"");
      buffer[index++] = chr;
      if (index == (max_buffer-1))
         index = 0;
      Serial.print(buffer);
      Serial.println("\"");
      }
}
```

The datasheet also shows how to control the pins in a faster way by setting certain port registers to **0 (Low)** or **1 (High)**.

### Port Manipulation Comparison ([megaTinyCore](https://github.com/SpenceKonde/megaTinyCore))

Here I use **PA3** for example:

<div style='overflow-x:auto;'>
    <table>
        <thead>
            <tr>
                <th>Function</th>
                <th>Register</th>
                <th>Arduino</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row">Output</th>
                <td>
                    PORTA.DIRSET = PIN3_bm;<br/>
                    PORTA.OUTSET = PIN3_bm;
                </td>
                <td>pinMode(4, OUTPUT);</td>
            </tr>
            <tr>
            <th scope="row">Input</th>
                <td>PORTA.DIRCLR = PIN3_bm;</td>
                <td>pinMode(4, INPUT);</td>
            </tr>
            <tr>
                <th scope="row">High</th>
                <td>PORTA.OUT |= PIN3_bm;</td>
                <td>digitalWrite(4, HIGH);</td>
            </tr>
            <tr>
                <th scope="row">Low</th>
                <td>PORTA.OUT &= ~PIN3_bm;</td>
                <td>digitalWrite(4, LOW);</td>
            </tr>
        </tbody>
    </table>
</div>

There are two ways to specify the ATtiny 412 pins in Arduino IDE. One is to use their original names shown below:

### ATtiny 412 Pins

![ATtiny412_pins.png](./img/ATtiny412_pins.png)

![ATtiny412_pin_table.png](./img/ATtiny412_pin_table.png)

#### [ATtiny412_blink.ino](https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_blink)

```arduino
#include <avr/io.h>

#define LED_AVR PIN3_bm //PA3: LED pin

void setup() {
   PORTA.DIRSET = LED_AVR;
}

void loop() {
  PORTA.OUT |= LED_AVR;
  delay(100);
  PORTA.OUT &= ~LED_AVR;
  delay(100);
 }
```

The other is to map the ATtiny 412 pins to the Arduino pin numbers with the [megaTinyCore](https://github.com/SpenceKonde/megaTinyCore) library installed so that we can simplify the code shown below:

### ATtiny 412 Pins (mapped to Arduino pin numbers)

![ATtiny412_x12_pin.gif](./img/ATtiny412_x12_pin.gif)

#### [ATtiny412\_blink\_simplified.ino](https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_blink_simplified)

```arduino
#define LED_AVR 4 //PA3: LED pin

void setup() {
  pinMode(LED_AVR, OUTPUT)
}

void loop() {
  digitalWrite(LED_AVR, HIGH);
  delay(100);
  digitalWrite(LED_AVR, LOW);
  delay(100);
}
```

### Set up Programming Environment ([megaTinyCore](https://github.com/SpenceKonde/megaTinyCore) + [pyUPDI](https://github.com/mraardvark/pyupdi) + Arduino IDE)

#### Step 1. Connect both boards to a USB hub and install both [VCP](https://www.ftdichip.com/Drivers/VCP.htm) and [D2XX](https://www.ftdichip.com/Drivers/D2XX.htm) drivers for a computer to detect FTDI devices (p.s. [Wacom drivers](https://www.wacom.com/en-us/support/product-support/drivers) might affect the FTDI detection.)

![board_ports.jpg](./img/board_ports.jpg)

#### Step 2. Type `ls /dev/tty.*` command in the terminal to find out both FTDI and UPDI ports

Here:

- FTDI Host Board: `/dev/tty.usbserial-D3072T22`

- UPDI Programmer Board: `/dev/tty.usbserial-D3072T1T`

![FTDI_UPDI_ports.png](./img/FTDI_UPDI_ports.png)

#### Step 3. Install [megaTinyCore](https://github.com/SpenceKonde/megaTinyCore) Arduino library

##### Option 1.

Copy and paste this URL **http://drazzy.com/package_drazzy.com_index.json** from [Boards Manager Installation](https://github.com/SpenceKonde/megaTinyCore/blob/master/Installation.md) section of the megaTinyCore library to the Arduino preference.

![Arduino_preference.png](./img/Arduino_preference.png)

After that, type the **megaTinyCore** keyword in the **Tools -> Board -> Board Manager** and install it.

![Arduino_board_manager.png](./img/Arduino_board_manager.png)

##### Option 2.

Download [megaTinyCore](https://github.com/SpenceKonde/megaTinyCore) library and unzip it to (MAC) **/Users/USER_NAME/Documents/Arduino/hardware** (create one if there is no **hardware** folder).

![megaTinyCore_hardware.png](./img/megaTinyCore_hardware.png)

#### Step 4. Add the **build path** to the Arduino **preference.txt** for saving the compiled **hex** file

Click the link shown in the Arduino preference to enter the **preference.txt** and add the following new line.

![Arduino_add_build_path.png](./img/Arduino_add_build_path.png)

#### Step 5. Restart Arduino IDE and choose which type of the ATtiny boards you want to program in the tool menu and its settings to start programming

![Arduino_board.png](./img/Arduino_board.png)

![ATtiny412_setting.png](./img/ATtiny412_setting.png)

#### Step 6. Start programming the code in Arudino IDE and compile it to generate a **hex** file in the **build** subfolder

[How to control the I/O of an ATtiny 412 chip by programming its registers can be found in the previous datasheet section.](#read-a-microcontroller-datasheet)

After finishing coding, click the **verify button** to comile the **hex** file which will be uploaded via **pyupdi** libray in the terminal.

![Arduino_hex.png](./img/Arduino_hex.png)

#### Step 7. Install [Python3 with pyenv](https://realpython.com/intro-to-pyenv/)

pyenv is a wonderful tool for managing multiple Python versions. Even if you already have Python installed on your system, it is worth having pyenv installed so that you can easily try out new language features or help contribute to a project that is on a different version of Python.

Follow the order below to install python properly:

1. `git clone https://github.com/pyenv/pyenv.git ~/.pyenv`
  download pyenv from GitHub

2. `echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc`
  assign its envrionment paths in the **.zshrc** (or **.bashrc**) profile

3. `echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshrc`

4. `echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n eval "$(pyenv init -)"\nfi ' >> ~/.zshrc`

5. `exec "$SHELL"`

6. `pyenv install 3.7.7`
  install the specific python version you prefer (Here: 3.7.7)

7. `pyenv global 3.7.7`
  switch to the installed version (Here: 3.7.7)

Other useful pyenv commands:

- `pyenv versions` list all the installed versions

- `pyenv version` list the version in use

- `pyenv uninstall 3.7.7` uninstall certain version (Here: 3.7.7)

![pyenv.png](./img/pyenv.png)

#### Step 8. Install [pyserial](https://github.com/pyserial/pyserial) and [pyupdi](https://github.com/mraardvark/pyupdi) packages via [pip](https://pip.pypa.io/en/stable/reference/pip_install/) command

1. `pip install pyserial` install the pyserial package

2. `pip install https://github.com/mraardvark/pyupdi/archive/master.zip` install the pyupdi package

3. `pyupdi` check pyupdi usage

Other commands for checking installed python packages/modules:

- `pip freeze` list all the installed packages

- `pip freeze | grep pyserial` list the certain installed package (Here: pyserial)

![pip.png](./img/pip.png)

#### Step 9. Upload the compiled hex file to the ATtiny 412 board via [pyupdi](https://github.com/mraardvark/pyupdi)

1. `ls /dev/tty.*`
  find out which port is the UPDI programmer board (Here: **/dev/tty.usbserial-D3072T1T**)

2. `pyupdi -d tiny412 -b 115200 -c /dev/tty.usbserial-D3072T1T -f /Users/USER_NAME/Documents/Arduino/build/ATtiny412_blink.ino.hex -v`
  upload the compiled [code](https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_blink) to the ATtiny 412 board with 115200 baud rate

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/pyupdi_upload_code.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

### End Result

The board I redrew finally works after I programmed it.

#### [ATtiny412_blink.ino](https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_blink): blink the LED

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/ATtiny412_blink-small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

#### [ATtiny412\_blink\_button.ino](https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_blink_button): blink the LED if the button is pressed

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/ATtiny412_button-small.mp4" type="video/mp4" />
        </video>
    </div>
</center>

```arduino
#include <avr/io.h>

#define LED_AVR PIN3_bm //PA3: LED pin
#define BUTTON_AVR PIN6_bm //PA6: Button pin

void setup() {
   PORTA.DIRSET = LED_AVR;
   PORTA.DIRCLR = BUTTON_AVR;
}

void loop() {
  bool state = PORTA.IN & BUTTON_AVR; // read the state of a pin
  switch(state) {
    case 0:
    PORTA.OUT &= ~LED_AVR;
    break;
    case 1:
    PORTA.OUT |= LED_AVR;
    break;
    default:
    PORTA.OUT &= ~LED_AVR;
    break;
  }
 }
```

---

## Group Assignment: Compare the performance and development workflows between different AVR boards

### [Link to Documentation Page](https://aaltofablab.gitlab.io/fab-academy-2020/embedded-programming/)

### AVR Programming: ATtiny 412 + avr-gcc + pyupdi + Terminal

#### Step 1. Set up Programming Environment

I followed this [avr-gcc & avrdude makefile tutorial | Compile C & upload to AVR board (mac)](https://youtu.be/iKqLbbyPydI) and adjusted some parameters to generate **hex** code from a **elf** file (which is generated from a **c** file) via **avr-gcc** and then upload the **hex** file to the ATtiny 412 board via **pyupdi**.

Follow the order below to install **avr-gcc** properly:

1. `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"`
  install Homebrew

2. `xcode-select --install`
  install Xcode

3. `brew tap osx-cross/avr`

4. `brew install avr-gcc` install [avr-gcc](https://github.com/osx-cross/homebrew-avr) & install [AVR Libc](https://www.nongnu.org/avr-libc/)

5. `brew install avrdude` install [AVR Downloader/Uploader](https://www.nongnu.org/avrdude/)

#### Step 2. Start Programming **C** File

##### [hello.412.blink.c](https://gitlab.com/cv47522/avr-code/-/blob/master/ATtiny/c-code/hello.412.blink.c)

```c
#include <avr/io.h>
#include <util/delay.h>

int main(void) {
    PORTA.DIRSET = PIN3_bm;// or PORTA.DIR = 0b01000000; // Use LED: PA3 as Output
    PORTA.DIRCLR = PIN6_bm;// Use Button: PA6 as Input

    while (1) {
        PORTA.OUT |= LED_AVR;
        _delay_ms(500);
        PORTA.OUT &= ~LED_AVR;
        _delay_ms(500);
    }
}
```

#### Step 3. Create & Edit a **Makefile**

- **INCLUDE**, **PACK**: Download the add-on packs of latetest AVR chips from [Microchip Packs Repository](http://packs.download.atmel.com/) (**Atmel ATtiny Series Device Support** (1.8.332)), rename the file type from **.atpack** to **.zip** and unzip the pack to any diretory you like.

##### [Makefile](https://gitlab.com/cv47522/avr-code/-/blob/master/ATtiny/c-code/Makefile)

```makefile
FILENAME      = hello.412.blink
PORT 	      = /dev/tty.usbserial-D3072T1T
DEVICE 	      = attiny412
PYUPDI_DEVICE = tiny412
# PROGRAMMER   = jtag2updi
BAUD 	      = 115200
# For adding device packs of the latest chips (e.g., ATtiny412, 1614...)
INCLUDE       = "/Users/USER_NAME/Documents/AVR/Atmel.ATtiny_DFP.1.8.332/include"
PACK	      = "/Users/USER_NAME/Documents/AVR/Atmel.ATtiny_DFP.1.8.332/gcc/dev/attiny412"
COMPILE       = avr-gcc -Wall -Os -DF_CPU=20000000 -mmcu=${DEVICE} -I ${INCLUDE} -B ${PACK}

default: compile upload clean

compile:
	${COMPILE} -c ${FILENAME}.c -o ${FILENAME}.o
    //highlight-next-line
	${COMPILE} -o ${FILENAME}.elf ${FILENAME}.o  # Problem: Can't generate .elf from .o file.
	avr-objcopy -j .text -j .data -O ihex ${FILENAME}.elf ${FILENAME}.hex
	avr-size --format=avr --mcu=${DEVICE} ${FILENAME}.elf

upload:
	# avrdude -v -p ${DEVICE} -c ${PROGRAMMER} -P ${PORT} -b ${BAUD} -U flash:w:${FILENAME}.hex:i
	pyupdi -d ${PYUPDI_DEVICE} -b ${BAUD} -c ${PORT} -f ${FILENAME}.hex -v

clean:
	rm ${FILENAME}.o
	rm ${FILENAME}.elf
	rm ${FILENAME}.hex
```
![arduino_avr_gcc.png](./img/arduino_avr_gcc.png)

![elf.png](./img/elf.png)

#### Step 4. Upload the code via **Makefile**

Since the command for uploading the **hex** file to the ATtiny 412 board through **pyupdi** has been defined in the **upload** section of the **Makefile**, we just need to type the following commands in the terminal with the ATtiny 412 board connected to the UPDI programmer introduced [above](#set-up-programming-environment-megatinycore--pyupdi--arduino-ide).

```bash
cd path/to/hex/and/Makefile/file
make
```

![avr-gcc_folder.png](./img/avr-gcc_folder.png)
