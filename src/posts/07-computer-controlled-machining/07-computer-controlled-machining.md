---
visibility: true
weekId: 7
title: "Computer-Controlled Machining"
publishDate: "2020-03-11"
weekImg: "07-CNC_hero-shot.jpg"
tags: "#Fusion360 #CNC #Mach 3"
assignment: "(Group) 1. Test runout, alignment, speeds, feeds, and toolpaths for your machine. (Individual) 2. Make (design+mill+assemble) something big."
download: "https://gitlab.com/cv47522/fab-academy/-/tree/master/src/posts/07-computer-controlled-machining/source_files"
---
```toc
# This code block gets replaced with the Table of Content
```

---

## Group Assignment: Characterize Our CNC Machine

### [Link to Documentation Page](https://aaltofablab.gitlab.io/fab-academy-2020/computer-controlled-machining/)

---

## Individual Assignment: Chair & Rotational Table CNC Milling

### Materials

<div style='overflow-x: auto;'>
    <table>
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">1</th>
            <td>CNC Milling Machine: Recontech 1312 (<a href='https://www.cnc.fi/recontech-1312.html'>Manual</a>)</td>
        </tr>
        <tr>
            <th scope="row">2</th>
            <td>6mm Two-Flute Square Flat Milling Bit</td>
        </tr>
        <tr>
            <th scope="row">3</th>
            <td>15mm Plywood</td>
        </tr>
        <tr>
            <th scope="row">4</th>
            <td>12mm Fiberboard</td>
        </tr>
        </tbody>
    </table>
</div>

### Design Tool: Fusion 360

3D Designing with Fusion 360: **Learn more from [Week 2: Computer-Aided Design](https://fabacademy.wantinghsieh.com/assignment/02-computer-aided-design)**

### Generate Bit CAM-Toolpath in Manufacture Section of Fusion 360

After finishing designing the chair and the table, I unfolded their parts and placed them within one piece of flat plywood respectively.

Chair:
![Chair_Assemble.png](./img/Chair_Assemble.png)
![CNC_Chair_1.png](./img/CNC_Chair_1.png)

Rotational Table:
![Rotational-Table_Assemble_1.png](./img/Rotational-Table_Assemble_1.png)
![CNC_Rotational-Table_1.png](./img/CNC_Rotational-Table_1.png)
![CNC_Rotational-Table_2.png](./img/CNC_Rotational-Table_2.png)

The bit I used for milling the rotational table (12mm fiberboard) and the chair (15mm Plywood) is **6mm** thick and its shape is **two-flute square flat**.

![6mm_2Flute_Square_Flat_Bit.jpg](./img/6mm_2Flute_Square_Flat_Bit.jpg)

I followed the steps below to generate the toolpaths:

1. Select **2D Contour** within the **Manufacture** section.
![cnc_fusion_step_1_2d_contour.png](./img/cnc_fusion_step_1_2d_contour.png)

2. Add the bit to the **Tool** by typing correspondent parameters.

General:
![cnc_fusion_step_2_bit_setup_1.jpg](./img/cnc_fusion_step_2_bit_setup_1.jpg)

Cutter:
![cnc_fusion_step_2_bit_setup_2.jpg](./img/cnc_fusion_step_2_bit_setup_2.jpg)
![sch_bit.png](./img/sch_bit.png)

Cutting data:
![cnc_fusion_step_2_bit_setup_3.jpg](./img/cnc_fusion_step_2_bit_setup_3.jpg)

Cutting speed & Tool Feed Calculation:
![cnc_speed_calculation.jpg](./img/cnc_speed_calculation.jpg)

Post processor:
![cnc_fusion_step_2_bit_setup_4.jpg](./img/cnc_fusion_step_2_bit_setup_4.jpg)

3. Back to the **2D Contour** window to finish the settings of other tabs.

Geometry:
![cnc_fusion_step_3_2d_contour_geometry.png](./img/cnc_fusion_step_3_2d_contour_geometry.png)

Heights:
![cnc_fusion_step_3_2d_contour_heights.png](./img/cnc_fusion_step_3_2d_contour_heights.png)

Passes:
![cnc_fusion_step_3_2d_contour_passes.png](./img/cnc_fusion_step_3_2d_contour_passes.png)

Linking:
![cnc_fusion_step_3_2d_contour_linking.png](./img/cnc_fusion_step_3_2d_contour_linking.png)

4. Click **Generate** to get the toolpath.
![cnc_fusion_step_4_generate.png](./img/cnc_fusion_step_4_generate.png)

5. Click **Simulate** to watch if the spindle moves correctly.
![cnc_fusion_step_5_simulate.png](./img/cnc_fusion_step_5_simulate.png)

6. Click **Post Process** to set the correspondent post processor (Here: **Mach3Mill**) and export the **tap** file for milling.
![cnc_fusion_step_6_post_process.png](./img/cnc_fusion_step_6_post_process.png)

![cnc_fusion_step_6_post_process_save_tap.png](./img/cnc_fusion_step_6_post_process_save_tap.png)

### Fabrication: Recontech 1312 CNC Milling Machine

Following is the CNC machine I used for milling.

![cnc_machine.jpg](./img/cnc_machine.jpg)

The fabrication steps are:

0. Follow the manual to set up the CNC working environment.
![cnc_milling_step_0_setup_manual.jpg](./img/cnc_milling_step_0_setup_manual.jpg)
The correspondent buttons:
![cnc_maccnc_milling_step_0_mach3_UIhine.jpg](./img/cnc_milling_step_0_mach3_UI.jpg)

1. Use foams and the built-in vacuum table to suck/fix the plywood.
![cnc_milling_step_1_suck_sheet.jpg](./img/cnc_milling_step_1_suck_sheet.jpg)

2. Insert the bit into the spindle with 2 spanners.
![spanner.jpg](./img/spanner.jpg)

![cnc_milling_step_2_install_bit.jpg](./img/cnc_milling_step_2_install_bit.jpg)

3. Measure the focus of the z-axis by pressing the **Terän mittaus** button in Mach3 to touch the yellow magnet.
![cnc_milling_step_3_focus_z_axis.jpg](./img/cnc_milling_step_3_focus_z_axis.jpg)

![cnc_milling_step_3_mach_focus_z_axis.jpg](./img/cnc_milling_step_3_mach_focus_z_axis.jpg)

4. Set the milling origin to the **bottom left** corner by moving the spindle with **Left, Right, Up, Down Arrow** and **Page Up/Down** keys.
![spancnc_milling_step_4_set_origin_1.jpg](./img/cnc_milling_step_4_set_origin_1.jpg)

![spancnc_milling_step_4_set_origin_2.jpg](./img/cnc_milling_step_4_set_origin_2.jpg)

5. Open the **tap** file in Mach3.
![cnc_milling_step_5_mach_open_tap_file.jpg](./img/cnc_milling_step_5_mach_open_tap_file.jpg)

6. Click **Regen Toolpath**.
![cnc_milling_step_6_mach_regen_toolpath.jpg](./img/cnc_milling_step_6_mach_regen_toolpath.jpg)

7. Reduce the **Slow Jog Rate** to **20%** during the initial milling.
![cnc_milling_step_7_mach_show_jog_rate-.jpg](./img/cnc_milling_step_7_mach_show_jog_rate-.jpg)

8. Start milling by pressing the **red** button in the outside room.

<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/1-chair_small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

The inital cut depth:
<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/5-chair_small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

The max cut depth:
<center>
    <div class="col-lg-8 embed-responsive embed-responsive-16by9">
        <video controls class="embed-responsive-item">
            <source src="./img/6-chair_small.mp4" type="video/mp4" />
        </video>
    </div>
</center>
<br />

9. Gradually increase the **Slow Jog Rate** to **100%** during miiling.
![cnc_milling_step_7_mach_show_jog_rate+.jpg](./img/cnc_milling_step_7_mach_show_jog_rate+.jpg)

### Assembling & Sanding

After finishing milling, I followed the steps below to do the post-processing:

1. Separate the cutting parts with the following tool manually.
![cnc_cutting_tool.jpg](./img/cnc_cutting_tool.jpg)

2. Assemble the chair with screws and an electric drill.
![cnc_sanding_step_1_before_1.jpg](./img/cnc_sanding_step_1_before_1.jpg)

![cnc_sanding_step_1_before_2.jpg](./img/cnc_sanding_step_1_before_2.jpg)

3. Remove the rough wooden fibres with a sanding machine.
![cnc_sanding_step_3.jpg](./img/cnc_sanding_step_3.jpg)

4. Fix the chair for gluing with G clamps.
![cnc_sanding_step_4_add_glue.jpg](./img/cnc_sanding_step_4_add_glue.jpg)

5. Add the top sheet.
![cnc_sanding_step_5_add_top_sheet.jpg](./img/cnc_sanding_step_5_add_top_sheet.jpg)

6. Remove the extra glue with tissues.
![cnc_sanding_step_6_dry_glue.jpg](./img/cnc_sanding_step_6_dry_glue.jpg)

![cnc_sanding_step_6_remove_extra_glue.jpg](./img/cnc_sanding_step_6_remove_extra_glue.jpg)

7. Wait for drying.
![cnc_sanding_step_7_waite_dry_1.jpg](./img/cnc_sanding_step_7_waite_dry_1.jpg)

![cnc_sanding_step_7_waite_dry_2.jpg](./img/cnc_sanding_step_7_waite_dry_2.jpg)

8. Finish!
![chair_2.jpg](./img/chair_2.jpg)

![chair_model_2.jpg](./img/chair_model_2.jpg)

### End Result

![chair_table_1.jpg](./img/chair_table_1.jpg)

![chair_table_2.jpg](./img/chair_table_2.jpg)

![table_3.jpg](./img/table_3.jpg)

![table_8.jpg](./img/table_8.jpg)

![table_11.jpg](./img/table_11.jpg)

### Problem & Solution

The dust sometimes affected the sucking strength of the vacuum and therefore made the plywood move during milling, which was really dangerous. I needed to pause the machine to remove the dust and fix the sheet again evrey now and then before continuing milling.
![cnc_problem_weak_sucking.jpg](./img/cnc_problem_weak_sucking.jpg)
