---
visibility: true
weekId: 2
title: "Computer-Aided Design"
publishDate: "2020-02-25"
weekImg: "02-model.jpg"
tags: "#Fusion360"
assignment: "Model (raster, vector, 2D, 3D, render, animate, simulate, ...) a possible final project, compress your images and videos, and post it on your class page."
download: "https://gitlab.com/cv47522/fab-academy/-/tree/master/src/posts/02-computer-aided-design/source_files"
---
```toc
# This code block gets replaced with the Table of Content
```

---

## 3D Modelling Project

### Design Tool: [Fusion 360](https://www.autodesk.com/products/fusion-360/overview)

Since Fusion 360 is an integrated CAD, CAM, and CAE software, which makes it easier to design both 2D and 3D models within the same platform. For this week, I designed a hex-shaped pedal holder which is one of the components I am going to apply for my [final project](https://fabacademy.wantinghsieh.com/Final).

![00_model.png](./img/00_model.png)

---

### Design Process

The following steps indicate how I created this 3D model:

#### Step 1. Open change parameters dialogue

![01_change_params.png](./img/01_change_params.png)

#### Step 2. Input user-defined parameters

![02_params.png](./img/02_params.png)

#### Step 3. In sketch mode, create a circumscribed polygon

![03_sketch_polygon.png](./img/03_sketch_polygon.png)

#### Step 4. Assign the polygon type (e.g., pentagon, hexagon or octagon)

![04_sketch_create_polygon.png](./img/04_sketch_create_polygon.png)

#### Step 5. Input the user-defined parameter to its dimension

![05_sketch_hex_polygon.png](./img/05_sketch_hex_polygon.png)

#### Step 6. Offset the hexagon sketch

![06_sketch_offset.png](./img/06_sketch_offset.png)

#### Step 7. Finish the hexagon sketch

![07_sketch_finish_hex.png](./img/07_sketch_finish_hex.png)

#### Step 8. Extrude the hexagon sketch

![08_solid_extrude_hex.png](./img/08_solid_extrude_hex.png)

#### Step 9. Switch to the front view then create a new sketch with two circles and a rectangle

![09_sketch_front_circle.png](./img/09_sketch_front_circle.png)

#### Step 10. Project the hexagon body to the new sketch for easily creating its constrains

![10_sketch_project.png](./img/10_sketch_project.png)

#### Step 11. Extrude the new sketch which forms a lower tube

![11_solid_extrude_tube.png](./img/11_solid_extrude_tube.png)

#### Step 12. Use the circular pattern function to duplicate the lower tube

![12_solid_circular_pattern.png](./img/12_solid_circular_pattern.png)

#### Step 13. Select the features for duplicate and assign the quantity

![13_solid_duplicate_tube.png](./img/13_solid_duplicate_tube.png)

#### Step 14. Switch to the top view then create a new sketch with a rectangle

![14_sketch_rectangle.png](./img/14_sketch_rectangle.png)

#### Step 15. Extrude the rectangle sketch

![15_solid_extrude_rectangle.png](./img/15_solid_extrude_rectangle.png)

#### Step 16. Create a new sketch with a circle on the ectruded rectangle solid

![16_sketch_hole.png](./img/16_sketch_hole.png)

#### Step 17. Extrude the circle as a hole by cutting the cube

![17_solid_extrude_hole.png](./img/17_solid_extrude_hole.png)

#### Step  18. Add fillets to smooth the edges of the upper tube

![18_solid_add_fillet.png](./img/18_solid_add_fillet.png)

#### Step  19. Use the circular pattern function again to duplicate the upper tube by selecting the features and assigning the quantity

![19_solid_duplicate_tube_v2.png](./img/19_solid_duplicate_tube_v2.png)

#### Step  20. Finished!

![00_model.png](./img/00_model.png)
