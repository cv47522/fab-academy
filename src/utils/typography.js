import Typography from "typography";
// import kirkhamTheme from "typography-theme-kirkham";
import stowLakeTheme from "typography-theme-stow-lake";
// import oceanBeachTheme from "typography-theme-ocean-beach";

// const typography = new Typography(oceanBeachTheme);
const typography = new Typography(stowLakeTheme);

// const typography = new Typography({
//     baseFontSize: '18px',
//     baseLineHeight: 1.666,
//     headerFontFamily: ['Itim', 'Helvetica Neue', 'Segoe UI', 'Helvetica', 'Arial', 'sans-serif'],
//     bodyFontFamily: ['Georgia', 'serif'],
//   });

export default typography;
export const rhythm = typography.rhythm;