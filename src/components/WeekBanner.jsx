import React from 'react';
import { Helmet } from "react-helmet"
import JSONdata from '../data/assignments.json';

import { Jumbotron, Button } from 'reactstrap';

export default function WeekBanner({ weekId }) {

  const weekObj = JSONdata[weekId-1];

  return (
    <div>
      <Helmet>
        <meta charSet="utf-8" />
        <title>{ `Week ${weekId} - ${weekObj.name} | Wan-Ting Hsieh 謝宛庭 | Fab Academy 2020` }</title>
      </Helmet>

      <Jumbotron>
        <h1 className="display-3">
          Week {weekObj.weekId}: {weekObj.name}
          <a href={weekObj.image}>
            <img alt='' className='mx-3 week-avatar-img' src={weekObj.image} />
          </a>
        </h1>
        <p className="lead">{weekObj.tags}</p>
        <hr className="my-2" />
          <p className="">{weekObj.description}</p>
        <p className='text-secondary'>Assignments: <span className='font-italic font-weight-light'>{weekObj.assignment}</span></p>
        <p className='text-secondary'>{weekObj.date}</p>
        <p className="lead">
          <Button color="primary" href={weekObj.download}>View Source Files</Button>
        </p>
      </Jumbotron>
    </div>
  );
}