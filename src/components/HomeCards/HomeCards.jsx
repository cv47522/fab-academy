import React from 'react';
import JSONdatas from '../../data/assignments.json';
import './HomeCards.css';

import { Link } from "gatsby";
import {
  CardImg,
  CardBody,
  CardSubtitle,
  Card,
  Button,
  CardTitle,
  CardText,
  Row,
  Col,
  Container }
from 'reactstrap';

export default function HomeCards() {
  return (
    <div>
      <Container className='home-container my-5'>
        <Row>
          {JSONdatas.map(data => {
            return (
              <Col md="3" sm="6" xs="12" key={data.weekId}>
                <Card className='my-2'>
                  <CardImg top width='80%' className='home-card my-0' src={data.image} alt={data.name} />
                  <CardBody className='cardbody-bg'>
                    <CardTitle className='h5 text-primary'>Week {data.weekId}</CardTitle>
                    <CardSubtitle className='h5'>{data.name}</CardSubtitle>
                    <CardText className='text-black-50'>{data.tags}</CardText>
                    <Button className='btn-warning' tag={Link} to={`/${data.path}`}>More</Button>
                  </CardBody>
                </Card>
              </Col>
            );

            // console.log(data);
          })}

          </Row>
        </Container>
      </div>
  );
}
