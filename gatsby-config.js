module.exports = {
  siteMetadata: {
    siteUrl: `https://fabacademy.wantinghsieh.com`,
    title: `Fab Academy 2020`,
    author: `Wan-Ting Hsieh 謝宛庭`,
    keywords: [`fab academy`, `wan-ting hsieh`, `謝宛庭`, `new media art`, `新媒體藝術`, `react`, `gatsby`, `graphql`],
    description:
      `Wan-Ting Hsieh's Project Documentation for Fab Academy 2020. The static site is built with React.js + Gatsby + GraphQL + GitLab CI/CD.`
  },
  plugins: [
    // for Contentfull CMS
    // {
    //   resolve: `gatsby-source-contentful`,
    //   options: {
    //     spaceId: process.env.CONTENTFUL_SPACE_ID,
    //     accessToken: process.env.CONTENTFUL_ACCESS_TOKEN
    //   }
    // },
    { // for specifying the base directory of Posts images
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `posts`,
        path: `${__dirname}/src/posts`,
      },
    },
    { // for specifying the base directory of About images
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `static`,
        path: `${__dirname}/static`,
      },
    },
    // for automatically gernerating the sitemap
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-transformer-remark`, // access markdown posts
      options: {
        plugins: [
          {
            resolve: "gatsby-remark-embed-video",
            options: {
              width: 800,
              ratio: 1.77, // Optional: Defaults to 16/9 = 1.77
              height: 400, // Optional: Overrides optional.ratio
              related: false, //Optional: Will remove related videos from the end of an embedded YouTube video.
              noIframeBorder: true, //Optional: Disable insertion of <style> border: 0
              containerClass: 'embedVideo-container' //Optional: Custom CSS class for iframe container, for multiple classes separate them by space
            }
          },
          `gatsby-remark-responsive-iframe`,
          // for copying used files (e.g., videos) in markdown posts to the "public" directory
          `gatsby-remark-copy-linked-files`,
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 800, // set image max width in the post
              quality: 100,
              withWebp: true,
              loading: 'lazy',
              linkImagesToOriginal: true // disable/enable/link to original size image URL
            }
          },
          {
            resolve: `gatsby-remark-image-attributes`,
            options: {
              styleAttributes: true,
              dataAttributes: false
            }
          },
          // for automactically generating the list of table of contents from hearders in markdown posts
          `gatsby-remark-autolink-headers`,
          {
            resolve: `gatsby-remark-table-of-contents`,
            options: {
              exclude: "Table of Contents",
              tight: false,
              fromHeading: 1,
              toHeading: 6,
              className: "table-of-contents"
            },
          },
          { // for opening new tabs by clicking markdown URLs
            resolve: "gatsby-remark-external-links",
            options: {
              target: "_blank",
              rel: "nofollow"
            }
          },
          { // for highlighting markdown code blocks
            resolve: `gatsby-remark-prismjs`,
            options: {
              classPrefix: "language-",
              inlineCodeMarker: null,
              aliases: {},
              showLineNumbers: false,
              noInlineHighlight: false,
              languageExtensions: [
                {
                  language: "superscript",
                  extend: "javascript",
                  definition: {
                    superscript_types: /(SuperType)/,
                  },
                  insertBefore: {
                    function: {
                      superscript_keywords: /(superif|superelse)/,
                    },
                  },
                },
              ],
              prompt: {
                user: "root",
                host: "localhost",
                global: false,
              },
              escapeEntities: {},
            },
          },
          { // for adding custom block css classes
            resolve: "gatsby-remark-custom-blocks",
            options: {
              blocks: {
                gifSize: {
                  classes: "gifSize",
                },
                eqBlock: {
                  classes: "eqBlock",
                },
              },
            },
          }
        ]
      }
    },
    { // for tracking the number of website views
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-99560507-5",
      },
    },
    // for adding CSS objects in .jsx files
    `gatsby-plugin-emotion`,
    { // for customizing website font style
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    // for editing page metadata objects in React.js pages
    `gatsby-plugin-react-helmet`,
    // for automatically optimizing images in React.js pages
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    { // for editing favicon, name amd browser background color
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Fab Academy 2020`,
        short_name: `Fab Academy 2020`,
        start_url: `/`,
        background_color: `#343a40`,
        theme_color: `#343a40`,
        display: `standalone`,
        icon: `static/favicon.png`, // This path is relative to the root of the site.
      },
    },
    { // for visiting the website offline
      resolve: 'gatsby-plugin-offline',
      options: {
         workboxConfig: {
            globPatterns: ['**/static*']
         }
      }
   }
  ],
}
