const path = require('path');

// Goal 1: Generate a slug for each post: postName.md -> postname -> /blog/postName
// Goal 2: Generate the blog post page template
// Goal 3: Generate a new page for each post

// no longer necessary if using Contenful API
module.exports.onCreateNode = ({ node, actions }) => {
    const { createNodeField } = actions;

    if (node.internal.type == 'MarkdownRemark') {
        // reduce the link: https://nodejs.org/dist/latest-v14.x/docs/api/path.html#path_path_basename_path_ext
        const slug = path.basename(node.fileAbsolutePath, '.md');

        createNodeField({
            node,
            name: 'slug',
            value: slug
        });
        // for easily see parameters in the terminal
        // console.log(JSON.stringify(node, undefined, 4));
        // console.log('@@@@@@@@@@@@@@@@@@@@@@', slug);
    }
}



// 1. Get path to template
// 2. Get markdown data
// 3. Create new pages
module.exports.createPages = async ({ graphql, actions }) => {
    const { createPage } = actions;
    const weekTemplate = path.resolve('./src/templates/weekTemplate.jsx');
    const res = await graphql(`
        query {
            allMarkdownRemark {
                edges {
                    node {
                        fields {
                            slug
                        }
                    }
                }
            }
        }
    `);
    // const res = await graphql(`
    //     query {
    //         allContentfulBlogPost {
    //             edges {
    //                 node {
    //                     slug
    //                 }
    //             }
    //         }
    //     }
    // `);

    res.data.allMarkdownRemark.edges.forEach((edge) => {
        createPage({
            component: weekTemplate,
            path: `/assignment/${edge.node.fields.slug}`,
            context: {
                slug: edge.node.fields.slug
            }
        });
    });
    // res.data.allContentfulBlogPost.edges.forEach((edge) => {
    //     createPage({
    //         component: blogTemplate,
    //         path: `/blog/${edge.node.slug}`,
    //         context: {
    //             slug: edge.node.slug
    //         }
    //     });
    // });
}