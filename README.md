# Wan-Ting's Project Documentation for Fab Academy
![gitlab-cicd](https://img.shields.io/gitlab/pipeline/cv47522/fab-academy/master)
![Netlify Status](https://img.shields.io/netlify/ffcf1df7-ecfc-4d01-8a1f-2dd145c37e22)

## Static Project Website (built with React.js + [Gatsby](https://www.gatsbyjs.com/) + [GraphQL](https://graphql.org/) + GitLab CI/CD)

### <http://fabacademy.wantinghsieh.com>

## Installation

1. Run the following commands in order:

    ```bash
    cd /path/to/repository/folder
    npm install -g gatsby-cli  # install gatsby-cli tool globally
    npm install   # install node modules from package.json
    npm run start  # run the server locally
    ```

2. Type the link in a browser: <http://localhost:8000/>
